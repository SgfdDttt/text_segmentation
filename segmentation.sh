raw_text='outputs/raw_'$1
clipped_text='outputs/clipped_'$1
raw_dict='outputs/raw_dic_'$1
unsegmented='outputs/unsegmented_'$1
segmented_with_dict='outputs/segmented_with_dictionary_'$1
segmented_with_ngram='outputs/segmented_with_ngrams_'$1
segmented_with_ngram_no_hapax='outputs/segmented_with_ngrams_no_hapax_'$1
segmented_with_ngram_more_t_10='outputs/segmented_with_ngrams_more_than_10_'$1
out_dir="outputs"
mkdir -p $out_dir
autoencoder=$2

#fabrique le corpus segmenté (liste de phrases)
#cat $1 |awk '{if($1~"[0-9]"){gsub("[;]","");if($3~"[<{]"){print ""}else printf("%s ",$3)}}'|awk '{if(NF)print}' > $raw_text
python scripts/delete_punctuation_and_numbers_and_uncapitalize.py $1 > $raw_text
python scripts/select_sentence_range.py $raw_text 1 100 > $clipped_text
#extrait le dico
awk '{for(i=1;i<=NF;i++)w[$i]++}END{for(i in w)print i,w[i]}' $clipped_text > $raw_dict
#fabrique le corpus non segmenté
awk '{for(i=1;i<=NF;i++)printf("%s",$i);print""}' $clipped_text > $unsegmented

if true; then
  #counts ngrams of size number_1 to size number_2
  touch $out_dir"/dictionary.iter00"
  #python scripts/make_n_gram_counts.py $unsegmented 1 20 > $out_dir"/dictionary.iter00"
  num_batches=2
  temperature=1.0
  prev_iter=00
  cp $unsegmented $out_dir"/segmented.iter"$prev_iter
  #echo "BASELINE segmentation scores"
  #python scripts/eval.py -g $clipped_text < $unsegmented
  #echo "TOPLINE segmentation"
  #th scripts/segment_list_nbest.lua -i $clipped_text -d $raw_dict -N 1 -t 0.0 > $out_dir"/topline"
  #python scripts/eval.py -g $clipped_text < $out_dir"/topline"
  for iter in `seq -w 1 16`; do
    echo "ITER $iter"
    #temperature=`python scripts/temperature_schedule.py $iter`
    num_lines=`wc -l $unsegmented | awk '{print $1}'`
    increment=$[num_lines/num_batches]
    touch $out_dir"/segmented.iter"$iter
    for batch in `seq 1 $increment $num_lines`; do
      # current batch is located between a and b (included)
      a=$batch
      b=$[batch+increment-1]
      if [ $b -ge $num_lines ]; then
        b=$num_lines
      fi
      # text to parse
      touch $out_dir"/reference_text"
      touch $out_dir"/cypher_text"
      cat $unsegmented | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b' >> $out_dir"/cypher_text"
      # text to count words from
      cat $out_dir"/segmented.iter"$prev_iter | awk -v var_a="$a" 'NR>=1&&NR<var_a' >> $out_dir"/reference_text"
      cat $out_dir"/segmented.iter"$prev_iter | awk -v var_b="$b" -v var_lines="$num_lines" 'NR>var_b&&NR<=var_lines' >> $out_dir"/reference_text"
      # make counts
      python scripts/make_dict_counts.py $out_dir"/reference_text" > $out_dir"/dictionary_buffer"
      # parse
      #echo "temperature "$temperature
      th scripts/segment_list_nbest.lua -N 10 -d $out_dir"/dictionary_buffer" -i $out_dir"/cypher_text" -t $temperature -o $out_dir"/buffer_segmented"
      # put into this iter's segmented file
      cat $out_dir"/buffer_segmented" >> $out_dir"/segmented.iter"$iter 
      cat $clipped_text | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b' > $out_dir"/comparison_text"
      echo "batch "$batch": "`python scripts/eval.py -g $out_dir"/comparison_text" < $out_dir"/buffer_segmented" | awk 'NR==2 {print $1}'`
      rm $out_dir"/reference_text"
      rm $out_dir"/cypher_text"
      rm $out_dir"/buffer_segmented"
      rm $out_dir"/comparison_text"
    done # end for batch
    echo "final segmentation ITER "$iter
    python scripts/make_dict_counts.py $out_dir"/segmented.iter"$iter > $out_dir"/dictionary.iter"$iter
    th scripts/segment_list_nbest.lua -N 1 -d $out_dir"/dictionary.iter"$iter -i $unsegmented -t 0.0 -o $out_dir"/segmented.iter"$iter
    #python scripts/make_dict_counts.py $out_dir"/segmented.iter"$iter > $out_dir"/dictionary.iter"$iter
    python scripts/eval.py -g $clipped_text < $out_dir"/segmented.iter"$iter
    prev_iter=$iter
  done # end for iter
fi # end if false

# version with learning/unlearning
if false; then
  echo 'initial segmentation'
  temperature=1.0
  # iterate segmentation
  cat $unsegmented > $out_dir"/segmented.iter00"
  echo 'make ngram counts'
  python scripts/make_n_gram_counts.py $unsegmented 1 222 > $out_dir"/pos_dict_iter00" # all ngrams we might encouter
  awk '{print $1}' $out_dir"/pos_dict_iter00" > $out_dir"/master_dict" # all ngrams we might encouter
  echo "" > $out_dir"/neg_dict_iter00" # initial negative example dictionary. Is empty
  #cp $autoencoder $out_dir"/autoencoder.iter00.3"
  th scripts/init_autoencoder.lua -h 4 -l 1 -s $out_dir"/autoencoder.iter00.3" -c $unsegmented
  #echo "evaluation of initial model"
  #th scripts/autoencoder_gbzm_extension_evaluation.lua -c "../rnn_autoencoder/data/gbzm_ext_1000_no_spaces.txt" -n $out_dir"/autoencoder.iter00.3" 
  #pretraining?
  #th scripts/encoder_decoder_dictionary_training.lua -n $out_dir"/autoencoder.iter00.3" -s $out_dir"/autoencoder.iter00.3" -D $out_dir"/pos_dict_iter00" -b 128 -M 100 --epochs 32 #-g 0
  cp $out_dir"/pos_dict_iter00" $out_dir"/tmp"
  python scripts/random_dict.py $out_dir"/tmp" 0.01 > $out_dir"/pos_dict_iter00"
  echo "compute embeddings"
  th scripts/autoencoder_embeddings_on_dictionary.lua -n $out_dir"/autoencoder.iter00.3" -d $out_dir"/pos_dict_iter00" > $out_dir"/embeddings00"
  #th scripts/perfect_embeddings.lua -d $out_dir"/pos_dict_iter00" -s $out_dir"/embeddings_dict"
  #th scripts/embeddings_from_dict.lua -i $out_dir"/pos_dict_iter00" -d $out_dir"/embeddings_dict" > $out_dir"/embeddings00"
  clusters=`wc -l $out_dir/pos_dict_iter00 | awk '{print $1}'`
  #clusters=15
  echo "cluster data with "$clusters" clusters"
  python scripts/make_gmm.py $out_dir"/embeddings00" $clusters > $out_dir"/gmm00"
  prev_iter="00" # used to store previous iteration in its original formatting (with zero padding)
  echo "BASELINE segmentation scores"
  python scripts/eval.py -g $clipped_text < $out_dir"/segmented.iter00"
  num_batches=8
  num_lines=`wc -l $unsegmented | awk '{print $1}'`
  increment=$[num_lines/num_batches]
  if true; then
    for iter in `seq -w 1 16`; do
      echo "ITER $iter"
      # make segmentation
      #temperature=`python scripts/temperature_annealing.py $iter`
      temperature=1.0
      touch $out_dir"/segmented.iter"$iter
      for batch in `seq 1 $increment $num_lines`; do
        # current batch is located between a and b (included)
        a=$batch
        b=$[batch+increment-1]
        if [ $b -ge $num_lines ]; then
          b=$num_lines
        fi
        # text to parse
        touch $out_dir"/reference_text"
        touch $out_dir"/cypher_text"
        cat $unsegmented | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b { print }' >> $out_dir"/cypher_text"
        # text to perform stats on
        cat $out_dir"/segmented.iter"$prev_iter | awk -v var_a="$a" 'NR>=1&&NR<var_a { print }' >> $out_dir"/reference_text"
        cat $out_dir"/segmented.iter"$prev_iter | awk -v var_b="$b" -v var_lines="$num_lines" 'NR>var_b&&NR<=var_lines { print }' >> $out_dir"/reference_text"
        # make dictionary, embeddings and clusters
        python scripts/make_dict_counts.py $out_dir"/reference_text" > $out_dir"/buffer_dictionary"
        th scripts/autoencoder_embeddings_on_dictionary.lua -n $out_dir"/autoencoder.iter"$prev_iter".3" -d $out_dir"/buffer_dictionary" > $out_dir"/buffer_embeddings"
        python scripts/make_gmm.py $out_dir"/buffer_embeddings" $clusters > $out_dir"/buffer_gmm"
        th scripts/segment_autoencoder_nbest.lua -i $out_dir"/cypher_text" -t $temperature -N 10 -n $out_dir"/autoencoder.iter"$prev_iter".3" -G $out_dir"/buffer_gmm" > $out_dir"/buffer_segmented"
        # append result to current segmented file
        cat $out_dir"/buffer_segmented" >> $out_dir"/segmented.iter"$iter
        # evaluate segmentation
        cat $clipped_text | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b { print }' > $out_dir"/comparison_text"
        echo "batch "$batch": "`python scripts/eval.py -g $out_dir"/comparison_text" < $out_dir"/buffer_segmented" | awk 'NR==2 {print $1}'`
        # remove buffer files
        rm $out_dir"/reference_text"
        rm $out_dir"/cypher_text"
        rm $out_dir"/buffer_segmented"
        rm $out_dir"/comparison_text"
        rm $out_dir"/buffer_dictionary"
        rm $out_dir"/buffer_gmm"
        rm $out_dir"/buffer_embeddings"
      done # end for batch
      # update dictionary, embeddings and gmm from training segmentation
      python scripts/make_dict_counts.py $out_dir"/segmented.iter"$iter > $out_dir"/pos_dict_iter"$iter
      th scripts/autoencoder_embeddings_on_dictionary.lua -n $out_dir"/autoencoder.iter"$prev_iter".3" -d $out_dir"/pos_dict_iter"$iter > $out_dir"/embeddings"$iter
      clusters=8
      python scripts/make_gmm.py $out_dir"/embeddings"$iter $clusters > $out_dir"/gmm"$iter
      # produce this iteration's best segmentation
      th scripts/segment_autoencoder_nbest.lua -i $unsegmented -t 0.0 -N 1 -n $out_dir"/autoencoder.iter"$prev_iter".3" -G $out_dir"/gmm"$iter > $out_dir"/segmented.iter"$iter
      # evaluate segmentation
      echo "final segmentation ITER "$iter
      python scripts/eval.py -g $clipped_text < $out_dir"/segmented.iter"$iter
      # update network, dictionary, embeddings and gmm for next iteration
      python scripts/make_dict_counts.py $out_dir"/segmented.iter"$iter > $out_dir"/pos_dict_iter"$iter
      if [ $((10#$iter % 16)) -eq 17 ]; then
        th scripts/encoder_decoder_dictionary_training.lua -n $out_dir"/autoencoder.iter"$prev_iter".3" -s $out_dir"/autoencoder.iter"$iter".3" -D $out_dir"/pos_dict_iter"$iter -M 100 -b 128 -c 0.0 --epochs 32 #-g 0
      else
        cp $out_dir"/autoencoder.iter"$prev_iter".3" $out_dir"/autoencoder.iter"$iter".3"
      fi
      if false; then
        th scripts/autoencoder_embeddings_on_dictionary.lua -n $out_dir"/autoencoder.iter"$iter".3" -d $out_dir"/pos_dict_iter"$iter > $out_dir"/embeddings"$iter
        python scripts/make_gmm.py $out_dir"/embeddings"$iter $clusters > $out_dir"/gmm"$iter
      fi # end if false
      python scripts/make_neg_dict.py $out_dir"/pos_dict_iter"$prev_iter $out_dir"/pos_dict_iter"$iter > $out_dir"/neg_dict_iter"$iter
      pos_size=`python scripts/dictionary_size.py $out_dir"/pos_dict_iter"$iter`
      neg_size=`python scripts/dictionary_size.py $out_dir"/neg_dict_iter"$iter`
      echo "pos_size "$pos_size
      echo "neg_size "$neg_size
      # evaluate gmm
      #th scripts/autoencoder_gmm_gbzm_extension_evaluation.lua -c $unsegmented -n $out_dir"/autoencoder.iter"$iter".3" -g $out_dir"/gmm"$iter 
      #th scripts/autoencoder_gbzm_extension_evaluation.lua -c "../rnn_autoencoder/data/gbzm_ext_1000_no_spaces.txt" -n $out_dir"/autoencoder.iter"$iter".3"
      prev_iter=$iter
    done # end for iter
  fi # end if true/false
fi # end if true/false
