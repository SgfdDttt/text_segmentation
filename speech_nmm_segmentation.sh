input_dir=$1 # assume the list of files is in there and called 'list.txt'; gold boundaries are in there, called 'gold_boundaries.txt'
outdir="outputs/"
mkdir -p $outdir

#0.41
#0.5 (data from franken_100)
min_l=0.31
max_l=0.60
tolerance=0.01 # 1 frame at 100Hz
temperature=1.0
clusters=4

# initialization. Bounds -m and -M taken from gold_boundaries.
th scripts/random_segmentation.lua -d $input_dir -L $input_dir"list.txt" -m $min_l -M $max_l --boundaries $outdir"boundaries00"
#th scripts/init_audio_nmm.lua -d $input_dir -L $input_dir"list.txt" -s $outdir"autoencoder00" -E 004 -D 128 -l 1 -K 2
th scripts/audio_segmentation_eval.lua -i $outdir"boundaries00" -g $input_dir"gold_boundaries.txt" -t $tolerance
# cheat with some pretraining
#th scripts/audio_autoencoder_training.lua -d $input_dir -L $input_dir"list.txt" --boundaries $outdir"boundaries00" -m $min_l -M $max_l -s $outdir"autoencoder00" -D 128 -E 128 -l 1 --epochs 512 -b 128 -v 0.0 -K $clusters -p 1
#th scripts/train_nmm_on_audio_segments.lua -d $input_dir -L $input_dir"list.txt" -n $outdir"autoencoder00" -s $outdir"autoencoder00" --epochs 0000 -b 128 -E 64 -D 256 -K 8 -l 1 -m $min_l -M 1000 --boundaries $outdir"boundaries00" -v 0.00 -p 1
#cat $input_dir"gold_boundaries.txt" > $outdir"boundaries00"

# divide into batches
num_batches=10
num_lines=`wc -l $input_dir"list.txt" | awk '{print $1}'`
increment=$[num_lines/num_batches]
for batch in `seq 1 $increment $num_lines`; do
  a=$batch
  b=$[batch+increment-1]
  if [ $b -ge $num_lines ]; then
    b=$num_lines
  fi
  cat $input_dir"list.txt" | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b' > $outdir"list.batch"$batch
  cat $outdir"boundaries00" | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b' > $outdir"boundaries.iter00.batch"$batch
  th scripts/embeddings_on_audio.lua -d $input_dir -L $outdir"list.batch"$batch -B $outdir"boundaries.iter00.batch"$batch -o $outdir"embeddings.iter00.batch"$batch -n $outdir"autoencoder00"
done

prev_iter=00
# iteration
if true; then
  for iter in `seq -w 1 16`; do
    echo "ITER "$iter
    if [ $((10#$iter % 16)) -eq 17 ]; then
      echo "retrain"
      th scripts/train_on_audio_segments.lua -d $input_dir -L $outdir"list.txt" -n $outdir"autoencoder"$prev_iter -s $outdir"autoencoder"$iter --epochs 64 -b 32 -m $min_l -M $max_l --boundaries $outdir"boundaries"$prev_iter -o $outdir"embeddings"$iter -v 0.05
    else
      cp $outdir"autoencoder"$prev_iter $outdir"autoencoder"$iter
      #cp $outdir"embeddings"$prev_iter $outdir"embeddings"$iter
    fi
    for batch in `seq 1 $increment $num_lines`; do
      cat $outdir"embeddings.iter"$prev_iter".batch"$batch >> $outdir"embeddings.iter"$iter".batch"$batch
    done # done for other_batch
    #if [ $clusters -ge 2 ]; then
    #clusters=$[clusters/2]
    #fi
    echo "max number of clusters: "$clusters
    #for batch in `python scripts/random_batch_order.py $increment $num_lines`; do
    for batch in `seq 1 $increment $num_lines`; do
      echo "batch "$batch
      # current batch is located between a and b (included)
      a=$batch
      b=$[batch+increment-1]
      if [ $b -ge $num_lines ]; then
        b=$num_lines
      fi
      # speech to parse
      touch $outdir"reference_embeddings.iter"$iter".batch"$batch
      rm $outdir"reference_embeddings.iter"$iter".batch"$batch # erase old list of embeddings if present
      for other_batch in `seq 1 $increment $num_lines`; do
        if [ $other_batch -ne $batch ]; then
          cat $outdir"embeddings.iter"$iter".batch"$other_batch >> $outdir"reference_embeddings.iter"$iter".batch"$batch
        fi
      done # done for other_batch
      #echo "cluster"
      python scripts/make_gmm.py $outdir"reference_embeddings.iter"$iter".batch"$batch $clusters > $outdir"gmm.iter"$iter".batch"$batch
      echo "used "`head -2 $outdir"gmm.iter"$iter".batch"$batch | awk 'NR==2'`" clusters"
      #echo "segment"
      th scripts/segment_speech_nbest.lua -d $input_dir -L $outdir"list.batch"$batch -N 10 -t $temperature -m $min_l -M $max_l -n $outdir"autoencoder"$iter -G $outdir"gmm.iter"$iter".batch"$batch -o $outdir"embeddings.iter"$iter".batch"$batch > $outdir"boundaries.iter"$iter".batch"$batch
      rm $outdir"embeddings.iter"$iter".batch"$batch
      th scripts/embeddings_on_audio.lua -d $input_dir -L $outdir"list.batch"$batch -B $outdir"boundaries.iter"$iter".batch"$batch -o $outdir"embeddings.iter"$iter".batch"$batch -n $outdir"autoencoder"$iter
      cat $input_dir"gold_boundaries.txt" | awk -v var_a="$a" -v var_b="$b" 'NR>=var_a&&NR<=var_b' > $outdir"gold_tmp"
      th scripts/audio_segmentation_eval.lua -i $outdir"boundaries.iter"$iter".batch"$batch -g $outdir"gold_tmp" -t $tolerance | awk 'NR==2'
    done # done for batch
    echo "=========="
    touch $outdir"boundaries"$iter
    rm $outdir"boundaries"$iter
    for batch in `seq 1 $increment $num_lines`; do
      cat $outdir"boundaries.iter"$iter".batch"$batch >> $outdir"boundaries"$iter
    done
    echo "evaluate total segmentation"
    th scripts/audio_segmentation_eval.lua -i $outdir"boundaries"$iter -g $input_dir"gold_boundaries.txt" -t $tolerance | awk 'NR==2'
    echo "recompute embeddings, gmm and segmentation"
    touch $outdir"ref_embeddings"$iter
    rm $outdir"ref_embeddings"$iter # erase old list of embeddings if present
    th scripts/embeddings_on_audio.lua -d $input_dir -L $input_dir"list.txt" -B $outdir"boundaries"$iter -o $outdir"ref_embeddings"$iter -n $outdir"autoencoder00"
    python scripts/make_gmm.py $outdir"ref_embeddings"$iter $clusters > $outdir"gmm"$iter
    echo "used "`head -2 $outdir"gmm"$iter | awk 'NR==2'`" clusters"
    th scripts/segment_speech_nbest.lua -d $input_dir -L $input_dir"list.txt" -N 10 -t 0.0 -m $min_l -M $max_l -n $outdir"autoencoder"$iter -G $outdir"gmm"$iter -o $outdir"embeddings"$iter > $outdir"boundaries"$iter
    th scripts/embeddings_on_audio.lua -d $input_dir -L $input_dir"list.txt" -B $outdir"boundaries"$iter -o $outdir"embeddings"$iter -n $outdir"autoencoder"$iter
    th scripts/audio_segmentation_eval.lua -i $outdir"boundaries"$iter -g $input_dir"gold_boundaries.txt" -t $tolerance | awk 'NR==2'
    prev_iter=$iter
  done # done for iter
fi
