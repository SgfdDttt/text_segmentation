#!/usr/bin/python

import functools, math, sys

class OneGramDist(dict):
   def __init__(self, filename):
      self.gramCount = 0

      # assume that the dict contains a word, a space, a number (int or float), and anything after that is garbage
      for line in open(filename):
        word_and_count = line[:-1].split(' ')
        if not len(word_and_count) == 2:
           print('one-grams.txt is incorrectly formatted')
        word = word_and_count[0]
        count = word_and_count[1]
        try:
          self[word] = float(count) # log probability from autoencoder
        except ValueError:
          print('problem here')
          print(line)
          break
        self.gramCount += self[word]

   def __call__(self, key):
      if key in self:
         return float(self[key])
      else:
         print('error! unknown word ' + key)

singleWordProb = OneGramDist('one-grams.txt')
def wordSeqFitness(words):
   return sum((singleWordProb(w)) for w in words) # summing log probabilities

def memoize(f):
   cache = {}

   def memoizedFunction(*args):
      if args not in cache:
         cache[args] = f(*args)
      return cache[args]

   memoizedFunction.cache = cache
   return memoizedFunction

@memoize
def segment(word):
   if not word: return []
   word = word.lower() # change to lower case
   allSegmentations = [[first] + segment(rest) for (first,rest) in splitPairs(word)]
   return max(allSegmentations, key = wordSeqFitness)

def splitPairs(word, maxLen=20):
   return [(word[:i+1], word[i+1:]) for i in range(max(len(word), maxLen))]

@memoize
def segmentWithProb(word):
   segmented = segment(word)
   return (wordSeqFitness(segmented), segmented)

o=open(sys.argv[1])
for line in o:
   print(" ".join(segment(line[:-1])))
o.close() 
