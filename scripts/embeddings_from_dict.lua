opt=lapp[[
-d,--dictionary (default "") full embedding dictionary
-i,--input (default "") input dictionary
]]

local saved_state = torch.load(opt.dictionary)
local str_dict = saved_state.dict_str
local tensor_dict = saved_state.dictionary

for line in io.lines(opt.input) do
local iter = line:gmatch('%S+')
local word = iter()
local count = iter() + 0
for i=1,count do io.write(str_dict[word]) end
--[[
for i=1,count do
local out = tensor_dict[word]:clone()
local noise = torch.Tensor(out:size()):normal(0,0.001)
out = out + noise
local str = out[1]
for jj=2,out:size(1) do
str = str .. ' ' .. out[jj]
end -- end for jj
io.write(str .. '\n')
end -- end for i
--]]
io.flush()
end -- end for line
