package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'rnn'
pcall(require, '../../rnn/SeqLSTM') -- oberon patch
require 'nn'
local GMM = require 'scripts/gmm'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'

opt=lapp[[
-d,--directory   (default "")   directory containing the audio to parse
-L,--list        (default "")   list containing the file names
-N,--N            (default 5)    N for N best list
-t,--temperature   (default 1.0)    temperature for parsing
-n,--network      (default "")    network for embeddings
-G,--gmm           (default "")    GMM file
-o,--out_file      (default "")   output file for embeddings
-m,--min_len       (default 0.0)  minimum length of a segment (in seconds)
-M,--max_len       (default 10.0) maximum length of a segment (in seconds)
]]

local indices_to_sequence = function(start,stop)
  local out = utterance:sub(start+1,stop):clone()
  return out
end

local sequence_to_string = function(sequence_tensor) -- for table indexing purposes
  -- sequence_tensor is of dim seq_len x dimension
  local buffer = {}
  for ii=1,sequence_tensor:size(1) do
    local buf = {}
    for jj=1,sequence_tensor:size(2) do
      table.insert(buf, sequence_tensor[ii][jj])
    end -- end for jj
    table.insert(buffer, table.concat(buf,' '))
  end -- end for ii
  return table.concat(buffer, '\n')
end

local logprob = function(start,stop)
  local out
  if loss_dictionary[string.format('%d %d', start, stop)] then
    out = loss_dictionary[string.format('%d %d', start, stop)]
  else
    out = gmm:probability(net:audio_embedding(indices_to_sequence(start,stop)))
    out = math.log(out)
    loss_dictionary[string.format('%d %d', start, stop)] = out
  end
  return out
end

local log_likelihood
--[[ recursive version
log_likelihood = function(parse)
  local out = 0
  if llk_cache[table.concat(parse, ' ')] then
    out = llk_cache[table.concat(parse, ' ')]
  else
    if #parse == 2 then
      out = logprob(parse[1], parse[2]) 
    elseif #parse > 2 then
      local first_half = {}
      local second_half = {}
      -- anything nicer to split an array in half?
      for ii=1,math.floor(#parse/2) do
        table.insert(first_half, parse[ii])
      end
      for ii=math.floor(#parse/2),#parse do -- because it needs the first boundary
        table.insert(second_half, parse[ii])
      end
      out = log_likelihood(first_half) + log_likelihood(second_half)
    end -- end if
    llk_cache[table.concat(parse, ' ')] = out
  end -- end if llk_cache[parse]
  return out
end
--]]

---[[ iterative version to avoid stack overflow
log_likelihood = function(parse)
  local out = 0
  if #parse < 2 then return -math.huge end
  if llk_cache[table.concat(parse, ' ')] then
    out = llk_cache[table.concat(parse, ' ')]
  else
    -- compute llk linearly
    for ii=1,#parse-1 do
      out = out + logprob(parse[ii],parse[ii+1])
    end -- end for ii
    llk_cache[table.concat(parse, ' ')] = out
  end -- end if llk_cache[parse]
  return out
end
--]]

--[[
local nbest_sorted = function(parse_list, min_llk)
  -- parse list is a list of lists of indices, sorted by likelihood of parse (1 is best)
  min_llk = min_llk or -math.huge
  local new_min_llk = -math.huge
  local out = {}
  for ii=1,#parse_list do
    if (log_likelihood(parse_list[ii]) >= min_llk) then -- only add if llk high enough
      table.insert(out, parse_list[ii])
      new_min_llk = log_likelihood(parse_list[ii]) -- because parse_list is sorted
    else
      break -- because parse_list is sorted
    end -- end if
    if #out == opt.N then break end
  end -- end for ii
  return out, new_min_llk
end
--]]

local insert_into_sorted = function(tab, element) -- tab is sorted with best element at index 1
  local elem_likelihood = log_likelihood(element)
  local index = #tab
  while ( (index > 0) and (elem_likelihood > log_likelihood(tab[index])) ) do
    index = index - 1
  end -- end while index > 0 ...
  index = index + 1 -- this while loop goes one index too far up
  table.insert(tab, index, element)
end

local test_if_sorted = function(tab) -- for debugging purposes
  sorted = true
  for ii=2,#tab do
    --sorted = sorted and ( ( (log_likelihood(tab[ii])==-math.huge) and (log_likelihood(tab[ii-1])==-math.huge) ) or (log_likelihood(tab[ii]) <= log_likelihood(tab[ii-1])) ) -- test if they're not -inf
    sorted = sorted and ( log_likelihood(tab[ii]) <= log_likelihood(tab[ii-1]) ) -- test if they're not -inf
  end
  return sorted
end

local parse_string_fast
parse_string_fast = function(start,stop) -- parse the sequence that is in between start and stop, making segments of length at least min_l and at most max_l
  local all_parses = {}
  if parse_cache[string.format('%d %d',start,stop)] then
    all_parses = parse_cache[string.format('%d %d',start,stop)]
  else
    if start == stop then
      all_parses[1] = {stop}
    else
      for k=start+min_l,math.min(stop, start+max_l) do
        local best_here = parse_string_fast(k, stop)
        for i=1,math.min(opt.N,#best_here) do -- maybe 4N or N/2
          insert_into_sorted(all_parses, {start, unpack(best_here[i])})
          all_parses[opt.N + 1] = nil -- remove elements if list size exceeds parameter
          ---[[
          if not test_if_sorted(all_parses) then
          print('PROBLEM')
          for _,p in pairs(all_parses) do
          print(table.concat(p,' '))
          print(log_likelihood(p))
          end -- end for p
          end -- end if not test_if_sorted
          --]]
        end -- end for i
      end -- end for k
    end -- end if start==stop
    parse_cache[string.format('%d %d',start,stop)] = all_parses
  end -- end if parse_cache
  return all_parses
end

local softmax = function(parses, temperature) -- simplify and stabilize sampling
  -- parses is short so no need to optimize this
  temperature = temperature or 1.0
  local result = {}
  for ii,parse in pairs(parses) do -- collect log likelihoods (limit calls to log_likelihood)
    result[ii] = log_likelihood(parse)
  end
  local mean = 0
  local denom = 0
  for _,lkh in pairs(result) do -- compute mean log likelihood
    mean = mean + lkh
  end
  mean = mean / #parses
  for ii,lkh in pairs(result) do -- subtract mean for stability, divide by temperature, and exponentiate
    result[ii] = math.exp((lkh-mean)/temperature)
    denom = denom + result[ii]
  end
  for ii,lkh in pairs(result) do -- normalize
    result[ii] = result[ii] / denom
  end
  return result
end

local sample_parse = function(parses, random_seed, temperature)
  if #parses==0 then return nil end
  if random_seed then math.randomseed(random_seed) end
  temperature = temperature or 0.0 -- between > 0.01 (otherwise unstable numerical results) and +inf, modulates the probabilities. Low temperature -> most likely parse is selected, high temperature -> all parses equally likely.
  local resi = 0
  if temperature > 0.0 then
    local probs = softmax(parses, temperature)
    local random = math.random()
    repeat
      resi = resi + 1
      random = random - probs[resi]
    until (random < 0) or (resi == #probs)
  else
    local probs = softmax(parses, 1.0)
    resi = 1
    for ii in pairs(probs) do
      resi = ((probs[ii] > probs[resi]) and ii) or resi
    end -- end for i,prob
  end -- end if temperature > 0.0
  return parses[resi]
end

-- global variables
embed_file = nil
if opt.out_file and opt.out_file:len() > 0 then embed_file = io.open(opt.out_file, 'w') end
saved_state = torch.load(opt.network)
net = Coupler:new(saved_state.encoder, saved_state.encoderLSTM, saved_state.decoder, saved_state.decoderLSTM, saved_state.ngmm)
loss_dictionary = {} -- contains losses for each sequence, storing them as we go or all at once. Each key is a string: two indices separated by a blank space.
llk_cache = {} -- maps parses to their llk
parse_cache = {} -- maps strings (2 indices, like in loss_dictionary) to nbest parses
gmm = GMM:new(opt.gmm)
gmm:load_parameters()
utterance = nil
data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc()

-- HOW THIS WORKS
-- utterance is a global variable that represents the current utterance to segment. It's a seq_len x dimension tensor
-- parse is a list of indices located in between frames. This means a word is anything in between 2 indices.
-- The boundary before the first frame is at 0; as a result indices are comprised between 0 and seq_len.
-- we carry and update a list of instances of parse (the N-best parses)
-- max_l and min_l are global too, indicating upper and lower bounds on the lengths of the segments. This accelerates the parsing process.

for utt in pairs(data_loader.data) do
  utterance = data_loader.data[utt] -- utterance is a seq_len x dimension tensor
  local sr = data_loader.sampling_rates[utt]
  min_l, max_l = math.floor(opt.min_len*sr), math.ceil(opt.max_len*sr)
  loss_dictionary, llk_cache, parse_cache = {}, {}, {} -- because the caches are shared, and only refer to boundaries, not actual frames, so there could be confusion from one utterance to the next
  local parses = parse_string_fast(0,utterance:size(1))
  local parse = sample_parse(parses, os.time()+utt, opt.temperature) -- sample the final parse
  --[[
  print('utterance ' .. utt)
  for _,par in pairs(parses) do
    print('===')
    print(table.concat(par,' '))
    print(log_likelihood(par))
    print('===')
  end
  --]]
  --local out = (parse and table.concat(parse, ' ')) or (0 .. utterance:size(1))
  local boundaries = {}
  if parse and (#parse>0) then
    for ii,ind in pairs(parse) do
      table.insert(boundaries, ind/sr)
      if ii < #parse then
        local embedding = net:audio_embedding(indices_to_sequence(parse[ii],parse[ii+1]))
        for jj=1,embedding:size(1) do
          embed_file:write(embedding[jj] .. ' ')
        end -- end for jj
        embed_file:write('\n')
      end -- end if
    end -- end for ii,ind
  else 
    print('problem with utterance num ' .. utt)
    print('size or parses? ' .. parses and #parses or 'parses = nil')
  end -- end if
  io.write(table.concat(boundaries, ' ') .. '\n')
  --io.write(out .. '\n')
  io.flush()
  embed_file:flush()
end -- end for line
--[[
print('LOSS_DICTIONARY')
print(loss_dictionary)
--]]
embed_file:close()
