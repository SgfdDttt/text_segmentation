require 'nn'
require 'rnn'

local CorrelationTool = {}
CorrelationTool.__index = CorrelationTool

function CorrelationTool:new(dictionary_filename, char_to_int, int_to_char)
  local self = setmetatable({}, CorrelationTool)
  self.dictionary_filename = dictionary_filename
  self.char_to_int = char_to_int
  self.int_to_char = int_to_char -- ensure consistency with the autoencoder
  return self
end

function CorrelationTool:load_dictionary()
  if not self.dictionary_filename then print 'no dictionary specified'; return end
  -- open dict, create table of counts
  local dict = {}
  local total_count = 0
  for line in io.lines(self.dictionary_filename) do
    local word_and_count = {}
    for i in line:gmatch('%S+') do -- we expect there to be a word, a space, a count, and maybe after that some garbage
      word_and_count[#word_and_count+1] = i
    end -- end for i
    dict[ word_and_count[1] ] = word_and_count[2] + 0
    total_count = total_count + word_and_count[2]
  end -- end for line
  self.dictionary = dict
  self.total_count = total_count
  -- string to sequence table
  string_to_sequence = {}
  for string in pairs(self.dictionary) do
    sequence = {}
    for c in string:gmatch('.') do
      sequence[#sequence+1] = self.char_to_int[c]
    end -- end for c
    string_to_sequence[string] = sequence
  end -- end for string
  self.string_to_sequence = string_to_sequence
end

function CorrelationTool:compute_losses_on_dictionary(coupler, gpu)
  -- compute autoencoder negative log likelihood losses
  coupler.encoder:evaluate(); coupler.decoder:evaluate()
  coupler.encoder:remember('neither'); coupler.decoder:remember('neither')
  local criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
  local losses = {}
  for string, sequence in pairs(self.string_to_sequence) do
    -- The input sentences to the encoder. 
    local encInSeq = torch.Tensor({ sequence }):t(1,2)
    --if gpu then encInSeq:cuda() end
    -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
    local seq2 = {}
    for ii=1,#sequence do
      seq2[ii+1] = sequence[ii]
    end
    seq2[1] = self.char_to_int['<s>']
    local decInSeq = torch.Tensor({ seq2 }):t(1,2)
    --if gpu then decInSeq:cuda() end
    -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
    local seq2 = {}
    for ii=1,#sequence do
      seq2[ii] = sequence[ii]
    end
    seq2[#seq2+1] = self.char_to_int['<eos>']
    -- The decoder predicts one per timestep, so we split accordingly.
    local decOutSeq = nn.SplitTable(1, 1):forward(torch.Tensor({ seq2 }))
    --if gpu then decOutSeq:cuda() end
    -- Forward pass
    local encOut = coupler.encoder:forward(encInSeq)
    coupler:forwardConnect()
    local decOut = coupler.decoder:forward(decInSeq)
    local loss = criterion:forward(decOut, decOutSeq)
    losses[string]=loss
  end -- end for string, sequence
  self.losses = losses
end

function CorrelationTool:compute_correlation()
  -- linear regression
  local x,y = {},{}
  for string in pairs(self.dictionary) do
    x[#x+1] = -math.log(self.dictionary[string]) + math.log(self.total_count)
    y[#y+1] = self.losses[string]
  end -- end for ind,string
  if (#x>0) and (#y==#x) then
    linear_regression = {}
    local x_tens, y_tens = torch.Tensor(x), torch.Tensor(y)
    local xy, xx, yy = torch.cmul(x_tens,y_tens):sum(), torch.cmul(x_tens,x_tens):sum(), torch.cmul(y_tens,y_tens):sum()
    local x_mean, y_mean = x_tens:mean(), y_tens:mean()
    local n = x_tens:size(1)
    linear_regression[2] = (xy - n*x_mean*y_mean) / (xx - n*x_mean*x_mean) -- beta
    linear_regression[1] = y_mean - linear_regression[2] * x_mean -- alpha
    linear_regression[3] = (xy - n*x_mean*y_mean) / math.sqrt((xx - n*x_mean*x_mean)*(yy - n*y_mean*y_mean)) -- r
  end -- end if #x>0...
  return linear_regression
end

return CorrelationTool
