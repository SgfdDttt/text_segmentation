local opt=lapp[[
-d,--dictionary (default "")  description
-s,--save_file (default "") 
]]

local one_hot = function(n)
  local out = {}
  for i=1,dictionary_size do
    out[i] = 0
  end -- end for
  out[n] = 1
  return out
end
local binary = function(n)
  local out = {}
  local acc = n
  for i=1,log_size do
  out[i] = acc % 2
  acc = math.floor(acc/2)
  end -- end while
  return out
end
local letter_encoding = function(word)
  local out = {}
  for i=1,char_dict_size do
    out[i] = 0
  end -- end for
  for char in word:gmatch('.') do
    out[ char_to_int[char] ] = out[ char_to_int[char] ] + 1
  end
  return out
end
local dictionary = {} -- word -> count
local chars = {}
dictionary_size = 0
for line in io.lines(opt.dictionary) do
  local iter = line:gmatch('%S+')
  local word = iter()
  local count = iter()+0
  dictionary[word] = count
  dictionary_size=dictionary_size+1
  for char in word:gmatch('.') do
    chars[char] = true
  end
end -- end for line
char_to_int = {}
char_dict_size = 0
for char in pairs(chars) do char_dict_size=char_dict_size+1; char_to_int[char]=char_dict_size end
log_size = math.ceil(math.log(dictionary_size)/math.log(2)) + 1
index=1
emb_dict={}
emb_dict_str={}
scrambled_words = {}
for word in pairs(dictionary) do table.insert(scrambled_words, word) end
math.randomseed( os.time() )
math.random(); math.random(); math.random()
for ii=#scrambled_words,2,-1 do
  local jj = math.random(ii)
  scrambled_words[ii], scrambled_words[jj] = scrambled_words[jj], scrambled_words[ii]
end
for _,word in pairs(scrambled_words) do
--for word in pairs(dictionary) do
  local embedding = binary(index)
  --local embedding = letter_encoding(word)
  emb_dict[word]=torch.Tensor(embedding)
  local out = table.concat(embedding, ' ') .. '\n'
  emb_dict_str[word] = out
  --[[
  for ii=1,dictionary[word] do
    io.write(out)
  end -- end for ii
  io.flush()
  --]]
  index=index+1
end -- end for word

if opt.save_file then
torch.save(opt.save_file, {dictionary=emb_dict, dict_str=emb_dict_str})
end
