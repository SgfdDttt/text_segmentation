import sys
import math
iteration = int(sys.argv[1])
# annealing
start = 100
cooling_factor = 0.9
temperature = start*math.exp((iteration-1)*math.log(cooling_factor))
if temperature < 0.3:
  temperature = 0 # otherwise unstable in practice
print(str(temperature))
