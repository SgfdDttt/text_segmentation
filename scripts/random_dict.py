import sys
import random
word_counts = dict()
word_list = list()
for line in open(sys.argv[1], 'r'):
  word_and_count = line[:-1].split(' ')
  word_counts[word_and_count[0]] = word_and_count[1]
  word_list.append(word_and_count[0])
random_dict = dict()
size = int(len(word_list)*min(float(sys.argv[2]), 1.0))
while len(random_dict) < size:
  rand = random.randint(0, len(word_list)-1)
  random_dict[word_list[rand]] = word_counts[word_list[rand]]
  word_list.remove(word_list[rand])
for word in sorted(random_dict):
  print(word + ' ' + random_dict[word])
