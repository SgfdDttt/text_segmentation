import sys
ngram_counts = dict()
min_ngram_size = int(sys.argv[2])
max_ngram_size = int(sys.argv[3])
for line in open(sys.argv[1], 'r'):
  line = line.replace('\n','')
  for n in range(min_ngram_size, max_ngram_size+1):
    ngrams = [line[i:i+n] for i in range(len(line)-n+1)]
    for ngram in ngrams:
      if ngram not in ngram_counts:
        ngram_counts[ngram]=0
      ngram_counts[ngram]+=1
sorted_ngrams = ngram_counts.keys()
sorted_ngrams.sort()
for ngram in sorted_ngrams:
  print(ngram + ' ' + str(ngram_counts[ngram]))
