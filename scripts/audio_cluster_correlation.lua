package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
local GMM = require 'text_segmentation/scripts/gmm'
require 'gnuplot'

opt=lapp[[
-e, --embeddings (default "")
-G, --gmm (default "")
]]

print '-- load embeddings into list'
local str_embeddings = {}
print 'read file'
for line in io.lines(opt.embeddings) do
  str_embeddings[line] = true -- avoid repeat embeddings
end -- end for line
for _ in pairs(str_embeddings) do num_embeddings = (num_embeddings or 0) + 1 end
print('number of embeddings: ' .. num_embeddings)
print 'convert to tensor'
embeddings = {}
for embedding in pairs(str_embeddings) do
  local e = {}
  for c in embedding:gmatch('%S+') do
    table.insert(e,c)
  end -- end for c
  table.insert(embeddings, torch.Tensor(e):clone())
end -- end for embedding

print '-- load gmm'
local gmm = GMM:new(opt.gmm)
gmm:load_parameters()

-- stats
print 'relative_probs'
local relative_probs = {}
for ii=1,gmm.log_weights:size(1) do
  table.insert(relative_probs, gmm.log_weights[ii])
  --table.insert(relative_probs, math.exp(gmm.log_weights[ii]))
end -- end for ii
gnuplot.figure(1)
gnuplot.title(opt.gmm)
gnuplot.plot({'weights', torch.Tensor(relative_probs)})

print 'distance_plots'
local distance_plots = {}
for e_ind=1,#embeddings do
  local distances = {}
  for cluster=1,gmm.means:size(1) do
    local y = torch.cmul(gmm.scaling_coefs, (embeddings[e_ind] - gmm.scaling_means))
    distances[cluster] = (y - gmm.means[cluster])*(gmm.precision_matrices[cluster]*(y - gmm.means[cluster])) -- squared euclidean distance
    distances[cluster] = -distances[cluster]
    --distances[cluster] = math.exp(distances[cluster])
  end -- end for cluster
  table.insert(distance_plots, {string.format('embedding %d', e_ind), torch.Tensor(distances)})
end -- end for e_ind
gnuplot.figure(2)
gnuplot.title(opt.embeddings)
gnuplot.plot(unpack(distance_plots))

print 'largest clusters plots'
local x = {}
local y = {}
for e_ind=1,#embeddings do
    local r = torch.cmul(gmm.scaling_coefs, (embeddings[e_ind] - gmm.scaling_means))
    x[e_ind] = (r - gmm.means[1])*(gmm.precision_matrices[1]*(r - gmm.means[1])) -- squared euclidean distance
    x[e_ind] = -x[e_ind] + gmm.log_weights[1]
    x[e_ind] = math.exp(x[e_ind])
    y[e_ind] = (r - gmm.means[2])*(gmm.precision_matrices[2]*(r - gmm.means[2])) -- squared euclidean distance
    y[e_ind] = -y[e_ind] + gmm.log_weights[2]
    y[e_ind] = math.exp(y[e_ind])
    --distances[cluster] = math.exp(distances[cluster])
  --table.insert(distance_plots, {string.format('embedding %d', e_ind), torch.Tensor(distances)})
end -- end for e_ind
gnuplot.figure(3)
gnuplot.title(opt.gmm)
gnuplot.plot({'relative probs', torch.Tensor(x), torch.Tensor(y), '+'})
