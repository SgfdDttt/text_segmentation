local GMM = {}
GMM.__index = GMM

function GMM:new(filename)
  local self = setmetatable({}, GMM)
  self.filename = filename
  return self
end

function GMM:load_parameters()
  if not self.filename then print 'no file specified'; return end
  local f = assert(io.open(self.filename))
  local dimension = f:read() + 0
  local num_components = f:read() + 0
  local str_m = f:read()
  local tab_means = {}
  for m in str_m:gmatch('%S+') do
    table.insert(tab_means, m + 0.0)
  end
  local centering_means = torch.Tensor(tab_means)
  local str_s = f:read()
  local tab_scale = {}
  for s in str_s:gmatch('%S+') do
    table.insert(tab_scale, s + 0.0)
  end
  local scaling_coefs = torch.Tensor(dimension):zero() -- store diagonal matrix into a vector
  for ii in pairs(tab_scale) do
    scaling_coefs[ii] = 1.0/((tab_scale[ii] > 0) and tab_scale[ii] or 1)
  end
  local weights = {}
  local str_w = f:read()
  for w in str_w:gmatch('%S+') do
    table.insert(weights, w + 0.0)
  end
  local means = torch.Tensor(num_components, dimension):zero() -- one mean per column
  for k=1,num_components do
    local mean = f:read()
    local c = 1
    for coef in mean:gmatch('%S+') do
      means[k][c] = coef + 0.0
      c=c+1
    end -- end for coef
  end -- end for k
  local precision_matrices = torch.Tensor(num_components, dimension, dimension):zero() -- precision_matrices[i] is precision matrix for component i
  for k=1,num_components do -- each line is a matrix
    local matrix = f:read()
    local row, column = 1, 1
    local index = 1
    for coef in matrix:gmatch('%S+') do -- coefficients are separated by a space and are written left to right then top to bottom
      --[[
      precision_matrices[k][row][column] = coef + 0.0
      column=column+1
      if column>dimension then
        row=row+1
        column=1
      end -- end if
      --]]
      precision_matrices[k][index][index] = coef + 0.0
      index=index+1
    end -- end for coef
  end -- end for k
  local log_weights = torch.Tensor(num_components):fill(-dimension/2*math.log(2*math.pi))
  --[[
  print('log_weights1')
  print(log_weights)
  --]]
  for k=1,num_components do
    log_weights[k] = log_weights[k] + weights[k] -- weights are already in log scale
    --[[
    print('weights[k]')
    print(weights[k])
    --]]
    --local e = torch.eig(precision_matrices[k],'N')
    --local log_det = e:t(1,2)[1]:log():sum()
    --[[
    print('torch.log(precision_matrices[k])')
    print(torch.log(precision_matrices[k]))
    --]]
    local log_det = torch.log(precision_matrices[k]):trace()
    --print('log_det ' .. log_det)
    log_weights[k] = log_weights[k] + log_det/2
  end -- end for k
  --[[
  print('dimension')
  print(dimension)
  print('log_weights')
  print(log_weights)
  print('means')
  print(means)
  print('precision_matrices')
  print(precision_matrices)
  --]]
  self.dimension = dimension
  self.number_of_components = num_components
  self.scaling_means = centering_means
  self.scaling_coefs = scaling_coefs
  self.log_weights = log_weights
  self.means = means
  self.precision_matrices = precision_matrices
  f:close()
end

function GMM:probability(x) -- x is a tensor of size (dimension)
  assert(x:size():size()==1)
  assert(x:size(1)==self.dimension)
  local y=x:clone()
  if self.scaling_means and self.scaling_coefs then
    y = torch.cmul(self.scaling_coefs, (y - self.scaling_means))
  end
  local reduced = torch.Tensor(self.number_of_components, self.dimension):zero()
  reduced:ger(torch.Tensor(self.number_of_components):fill(1.0), y) -- line i contains x
  --[[
  print('reduced1')
  print(reduced)
  --]]
  reduced = reduced - self.means -- line i contains x-mu_i
  --[[
  print('reduced2')
  print(reduced)
  --]]
  local exponents = torch.Tensor(self.number_of_components):zero()
  for k=1,self.number_of_components do
    exponents[k] = (reduced[k])*(self.precision_matrices[k]*reduced[k]) -- matrix multiplication followed by dot product
  end -- end for k
  --[[
  print('exponents')
  print(exponents)
  --]]
  local probs = (-exponents/2) + self.log_weights
  --[[
  print('probs')
  print(probs)
  --]]
  return probs:exp():sum()
end

return GMM
