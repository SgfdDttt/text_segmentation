--[[
--Example of "coupled" separate encoder and decoder networks using SeqLSTM, e.g. for sequence-to-sequence networks.
--]]

require 'nn'
require 'rnn'
local DataLoader = require '../../rnn_autoencoder/data_loader'

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-c,--corpus              (default "data/input.txt") corpus file name for training
-h,--hidden_size        (default 128)             number of units in hidden layers of RNNs
-l,--layers             (default 2)               number of layers for both nets
]]

-- load data
local data_loader = DataLoader:new(opt.corpus)
data_loader:create_dataset()
data_loader:load_dataset()
-- add 2 wors to vocab, one for the start of sentence <s>, one for the end of sentence <eos>
data_loader.int_to_char[#data_loader.int_to_char + 1] = '<s>'
data_loader.char_to_int['<s>'] = #data_loader.int_to_char
data_loader.int_to_char[#data_loader.int_to_char + 1] = '<eos>'
data_loader.char_to_int['<eos>'] = #data_loader.int_to_char
print(data_loader.int_to_char)

torch.manualSeed(os.time())

-- Encoder
local enc = nn.Sequential()
enc:add(nn.LookupTable(data_loader:vocab_size(), opt.hidden_size))
local encLSTM = {}
for layer=1,opt.layers do
  encLSTM[layer] = nn.SeqLSTM(opt.hidden_size, opt.hidden_size)
  enc:add(encLSTM[layer])
end
--enc:add(nn.Sequencer(nn.L1Penalty(5e-6)))
--enc:add(nn.Dropout())
local params_enc = enc:getParameters()
params_enc:uniform(-0.01,0.01)
print(enc)

-- Decoder
local dec = nn.Sequential()
dec:add(nn.LookupTable(data_loader:vocab_size(), opt.hidden_size))
local decLSTM = {}
for layer=1,opt.layers do
  decLSTM[layer] = nn.SeqLSTM(opt.hidden_size, opt.hidden_size)
  dec:add(decLSTM[layer])
  --dec:add(nn.Dropout())
end
dec:add(nn.SplitTable(1, 3))
dec:add(nn.Sequencer(nn.Linear(opt.hidden_size, data_loader:vocab_size())))
dec:add(nn.Sequencer(nn.LogSoftMax()))
local params_dec = dec:getParameters()
params_dec:uniform(-0.01,0.01)
print(dec)

-- save model
torch.save(opt.save_file, {encoder = enc, encoderLSTM = encLSTM, decoder = dec, decoderLSTM = decLSTM, opt = opt, char_to_int = data_loader.char_to_int, int_to_char = data_loader.int_to_char})
