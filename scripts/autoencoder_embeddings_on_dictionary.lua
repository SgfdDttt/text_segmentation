Coupler = require '../../rnn_autoencoder/encoder_decoder_coupler'
require 'nn'
require 'rnn'
pcall(require,'../../rnn/SeqLSTM')

opt = lapp[[
-n,--network   (default "")   network to test: load a cpu net
-d,--dictionary    (default "") dictionary to compute embeddings on
-g,--gpu           (default -1) gpu
--]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

-- load saved state
save_state = torch.load(opt.network)

-- load trained encoder and decoder')
local coupler = Coupler:new(save_state.encoder, save_state.encoderLSTM, save_state.decoder, save_state.decoderLSTM)
if opt.gpu then
  coupler:cuda()
end

-- put dictionary into table
local dictionary = {} -- all words from input file
for line in io.lines(opt.dictionary) do
  local word_and_count = {}
  for i in line:gmatch('%S+') do
    table.insert(word_and_count, i)
  end -- end for i
  dictionary[ word_and_count[1] ] = word_and_count[2] + 0
end -- end for line

-- load character maps
local int_to_char = save_state.int_to_char
local char_to_int = save_state.char_to_int

-- convert dictionary words to sequences of indices
string_to_sequence = {}
for string in pairs(dictionary) do
  sequence = {}
  for c in string:gmatch('.') do
    sequence[#sequence+1] = char_to_int[c]
  end -- end for c
  string_to_sequence[string] = sequence
end -- end for string,prob

-- compute autoencoder negative log likelihood losses
local sorted_strings = {}
for string, _ in pairs(string_to_sequence) do
  sorted_strings[#sorted_strings+1]=string
end
table.sort(sorted_strings)
for jj,string in pairs(sorted_strings) do
  local sequence = string_to_sequence[string]
  local embedding = coupler:embedding_one_sequence(sequence)
  local out = {}
  for ii=1,embedding:size(1) do
    table.insert(out, embedding[ii])
  end -- end for ii
  for ii=1,dictionary[string] do
    io.write(table.concat(out, ' ') .. '\n')
  end
end -- end for string, sequence
--[[
local l1_loss = 0
for jj,string in pairs(sorted_strings) do
  local sequence = string_to_sequence[string]
  local embedding = coupler:embedding_one_sequence(sequence)
  for ii=1,embedding:size(1) do
    l1_loss = l1_loss + math.abs(embedding[ii])
  end -- end for ii
end -- end for string, sequence
l1_loss = l1_loss / 16
print(l1_loss)
--]]
