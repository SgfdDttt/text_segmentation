local GMM = require 'scripts/gmm'

opt=lapp[[
-d,--dictionary   (default "")   file containing the embeddings
-N,--N            (default 5)    N for N best list
-i,--input         (default "")     file to parse
-t,--temperature   (default 1.0)    temperature for parsing
-G,--gmm           (default "")    GMM file
]]

if opt.input:len()==0 then print('no input specified') end

local saved_state = torch.load(opt.dictionary)
local dictionary = saved_state.dictionary

local string_to_sequence = function(string)
  out = {}
  for char in string:gmatch('.') do
    table.insert(out, saved_state.char_to_int[char])
  end
  return out
end

local read_dictionary = function(string)
  out = {}
  for i in string:gmatch('%S+') do
    out[#out+1] = i
  end
  return out
end

local split_string = function(string, index)
  return {string:sub(1,index), string:sub(index+1)}
end

---[[ for debugging
local logprob = function(word)
  local epsilon = 1.5
  --[[
  local inp = dictionary[word]:clone()
  inp = inp + torch.Tensor(inp:size()):normal(0,1e-3)
  local out1 = gmm:probability(inp)
  --]]
  local out1 = gmm:probability(dictionary[word])
  out1 = math.log(out1)
  ---[[
  local out2 = 0
  for char in word:gmatch('.') do
    --local buf =  dictionary[char]:clone()
    --buf = buf + torch.Tensor(buf:size()):normal(0,1e-3)
    --out2 = out2 + math.log(gmm:probability(buf))
        --local buf = gmm:probability(dictionary[char])
    out2 = out2 + math.log(gmm:probability(dictionary[char])) -- epsilon
  end -- end for char
  --out2 = (out2 == -math.huge) and 0 or out2 -- HERE there can be a problem if for some reason out2 = -inf. Typically towards the end when no letters in the dictionary
  --]]
  ---[[
  local out = out1
  if out1 == -math.huge then -- because out2 can be > out1 even when out1 is not -inf
    out = out2
  end
  --]]
  --local out = math.max(out1,out2)
  print(word .. ' ' .. out)
  return out
  --return out1
  --return math.max(out1, out2)
end
--]]

--[[ with network and GMM
local logprob = function(word)
  local out
  if loss_dictionary[word] then
    out = loss_dictionary[word]
  else
    out = gmm:probability(net:embedding_one_sequence(string_to_sequence(word)))
    out = (out == 0 and -math.huge) or math.log(out)
    --print(word .. ' ' .. out)
    loss_dictionary[word] = out
  end
  return out
end
--]]

--[[ with network
local logprob = function(word)
  local out
  if loss_dictionary[word] then
    out = loss_dictionary[word]
  else
    out = net:loss_one_sequence(string_to_sequence(word), saved_state.char_to_int['<s>'], saved_state.char_to_int['<eos>'])
    loss_dictionary[word] = out
  end
  return out
end
--]]

local parse_log_likelihood
parse_log_likelihood = function(word_list)
  local out = 0
  if llk_cache[word_list] then
    out = llk_cache[word_list]
  else
    if #word_list == 1 then
      out = logprob(word_list[1]) 
    elseif #word_list > 1 then
      local first_half = {}
      local second_half = {}
      -- anything nicer to split an array in half?
      for ii=1,math.floor(#word_list/2) do
        first_half[#first_half+1] = word_list[ii]
      end
      for ii=math.floor(#word_list/2)+1,#word_list do
        second_half[#second_half+1] = word_list[ii]
      end
      out = parse_log_likelihood(first_half) + parse_log_likelihood(second_half)
    end -- end if
    llk_cache[word_list] = out
  end -- end if llk_cache[word_list]
  return out
end

local nbest_unsorted = function(parse_list, n) -- this can change the order of parse_list but won't destroy it
  local max_len = #parse_list -- parse list is a list of lists of strings. Each item is a parse
  local out = {}
  repeat
    local maxi = 1
    for ii=1,max_len do
      maxi = ((parse_log_likelihood(parse_list[ii]) > parse_log_likelihood(parse_list[maxi])) and ii) or maxi
    end
    out[#out+1] = parse_list[maxi]
    parse_list[maxi], parse_list[max_len] = parse_list[max_len], parse_list[maxi] -- exchange both parses
    max_len = max_len - 1
  until #out == math.min(n, #parse_list)
  return out
end

local nbest_sorted = function(parse_list, n, min_llk)
  -- parse list is a list of lists of strings, sorted by likelihood of parse (1 is best)
  min_llk = min_llk or -math.huge
  local new_min_llk = -math.huge
  local out = {}
  for ii=1,#parse_list do
    if (parse_log_likelihood(parse_list[ii]) >= min_llk) then -- only add if llk high enough
      table.insert(out, parse_list[ii])
      new_min_llk = parse_log_likelihood(parse_list[ii]) -- because parse_list is sorted
    else
      break -- because parse_list is sorted
    end -- end if
    if #out == n then break end
  end -- end for ii
  return out, new_min_llk
end

--[[ this function is slower than inser_into_sorted in practice, and the lists used are tipycally short (< 10) so we don't use it
local insert_into_sorted_dichotomic = function(tab, element) -- tab is sorted with best element at index 1
  if #tab == 0 then
    tab[1] = element
  else
    local upper = #tab + 1
    local lower = 0
    local elem_likelihood = parse_log_likelihood(element)
    repeat
      local pivot = math.floor((lower + upper)/2)
      if elem_likelihood > parse_log_likelihood(tab[pivot]) then
        upper = pivot
      else
        lower = pivot
      end -- end if 
    until upper - lower < 2 -- end while lower ~= upper
    table.insert(tab, upper, element)
  end -- end if #tab ...
end
--]]

local insert_into_sorted = function(tab, element) -- tab is sorted with best element at index 1
  if #tab == 0 then
    tab[1] = element
  else
    local elem_likelihood = parse_log_likelihood(element)
    local index = #tab
    while (index > 0) and (elem_likelihood > parse_log_likelihood(tab[index])) do
      index = index - 1
    end -- end while index > 0 ...
    index = index + 1 -- this while loop goes one index too far up
    table.insert(tab, index, element)
  end -- end if #tab ...
end

local test_if_sorted = function(tab) -- for debugging purposes
  sorted = true
  for ii=2,#tab do
    sorted = sorted and (parse_log_likelihood(tab[ii]) < parse_log_likelihood(tab[ii-1]))
  end
  return sorted
end

local parse_string_fast
parse_string_fast = function(string)
  local all_parses = {}
  if parse_cache[string] then
    all_parses = parse_cache[string]
  else
    if string:len() == 0 then
      all_parses[1] = {}
    else
      local min_llk = -math.huge -- keep in mind the best parses to avoid parsing some stuff
      for k=1,string:len() do
        local strings = split_string(string,k)
        if logprob(strings[1]) < min_llk then break end
        -- explanation: all the following are parses with the first cut after character at index k. Thus the llk of these parses can only be lower than the log prob of word s_1...s_k. So if logprob(s_1...s_k) is already too low no need to bother computing the parses of s_{k+1}...s_n
        local best_here = nbest_sorted(parse_string_fast(strings[2]), opt.N, min_llk - logprob(strings[1]))
        for i=1,math.min(opt.N,#best_here) do -- maybe 4N or N/2
          insert_into_sorted(all_parses, {strings[1], unpack(best_here[i])})
          all_parses[opt.N + 1] = nil -- remove elements with too low log likelihood
          --if not test_if_sorted(all_parses) then print('PROBLEM') end
        end -- end for i
        min_llk = (all_parses[opt.N] and parse_log_likelihood(all_parses[opt.N])) or -math.huge 
      end -- end for k
    end -- end if
    parse_cache[string] = all_parses
  end -- end if
  return all_parses
end

local softmax = function(parses, temperature) -- simplify and stabilize sampling
  -- parses is short so no need to optimize this
  temperature = temperature or 1.0
  local result = {}
  for ii,parse in pairs(parses) do -- collect log likelihoods (limit calls to parse_log_likelihood)
    result[ii] = parse_log_likelihood(parse)
  end
  local max = -math.huge
  local denom = 0
  for _,lkh in pairs(result) do -- compute max log likelihood
    max = (max > lkh) and max or lkh
  end
  for ii,lkh in pairs(result) do -- subtract max for stability, divide by temperature, and exponentiate
    result[ii] = math.exp((lkh-max)/temperature)
    denom = denom + result[ii]
  end
  for ii,lkh in pairs(result) do -- normalize
    result[ii] = result[ii] / denom
  end
  return result
end

local sample_parse = function(parses, random_seed, temperature)
  if random_seed then math.randomseed(random_seed) end
  temperature = temperature or 0.0 -- between > 0.01 (otherwise unstable numerical results) and +inf, modulates the probabilities. Low temperature -> most likely parse is selected, high temperature -> all parses equally likely.
  local resi = 0
  if temperature > 0.0 then
    local probs = softmax(parses, temperature)
    local random = math.random()
    repeat
      resi = resi + 1
      random = random - probs[resi]
    until (random < 0) or (resi == #probs)
  else
    local probs = softmax(parses, 1.0)
    resi = 1
    for ii in pairs(probs) do
      resi = ((probs[ii] > probs[resi]) and ii) or resi
    end -- end for i,prob
  end -- end if temperature > 0.0
  return parses[resi]
end

-- global variables
loss_dictionary = {} -- contains losses for each string, storing them as we go or all at once
llk_cache = {} -- maps parses to their llk
parse_cache = {} -- maps strings to nbest parses
gmm = GMM:new(opt.gmm)
gmm:load_parameters()

--[[ for debugging
total_count = 0
for line in io.lines(opt.dictionary) do
  loss_dictionary[ read_dictionary(line)[1] ] = read_dictionary(line)[2] + 0.0
  total_count = total_count + read_dictionary(line)[2]
end -- end for line
--]]


--local time = os.time()
counter = 0
for line in io.lines(opt.input) do
  counter = counter + 1
  local parses = parse_string_fast(line)
  --local parse = parses[1] -- print just the best parse
  local parse = sample_parse(parses, os.time()+counter, opt.temperature) -- sample the final parse
  --local ok, parse_try = pcall(sample_parse(parses, os.time()+counter, opt.temperature)) -- sample the final parse
  io.write(table.concat(parse, " ") .. '\n')
  io.flush()
end -- end for line
loss_dictionary = nil
llk_cache = nil
parse_cache = nil -- just in case it could be called from the same script with the values filled in by a previous call...
--time = os.time() - time
