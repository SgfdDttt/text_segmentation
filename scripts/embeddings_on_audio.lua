package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
local gnuplot = require 'gnuplot'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-o,--out_file          (default "out.md")   file to write embeddings
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-n,--network            (default "")              reload pretrained network
-B, --boundaries          (default "")
-b,--batch_size         (default 128)     batch size
-g,--gpu                (default -1) run on gpu
]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    print('using CUDA on GPU ' .. opt.gpu .. '...')
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

local save_state = torch.load(opt.network)

embeddings_file = assert(io.open(opt.out_file, 'w'))

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc_dictionary(opt.boundaries, 0.0, 1.0)

-- Coupler
local ed_coupler = Coupler:new(save_state.encoder, save_state.encoderLSTM, save_state.decoder, save_state.decoderLSTM, save_state.ngmm)
if opt.gpu then -- ship networks to GPU
  ed_coupler:cuda()
end

-- compute embeddings
ed_coupler.encoder:training() -- otherwise, it will forget the embeddings in between, thus those that we're interested in
for utterance=1,#(data_loader.data),opt.batch_size do 
  local data, num_frames = data_loader:make_batch(utterance, utterance+opt.batch_size-1)
  if opt.gpu then encInSeq:cuda() end
  -- Forward pass
  local embeddings = ed_coupler:embeddings(data, num_frames)
  for ii=1,embeddings:size(1) do
    local out = {}
    for jj=1,embeddings:size(2) do
      table.insert(out, embeddings[ii][jj])
    end
    embeddings_file:write(table.concat(out, ' ') .. '\n')
  end
  embeddings_file:flush()
end -- end for utterance
-- end compute embeddings
embeddings_file:close()
