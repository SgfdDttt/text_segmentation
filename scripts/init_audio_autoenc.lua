package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
local gnuplot = require 'gnuplot'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-h,--hidden_size        (default 128)             number of units in hidden layers of RNNs
-l,--layers             (default 2)               number of layers for both nets
]]

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc()
print('dimension: ' .. data_loader:dim())

-- Encoder
local enc = nn.Sequential()
enc:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.hidden_size)))
local encLSTM = {}
for layer=1,opt.layers do
  encLSTM[layer] = nn.SeqLSTM(opt.hidden_size, opt.hidden_size)
  enc:add(encLSTM[layer])
end
enc:remember('neither')

-- Decoder
local dec = nn.Sequential()
dec:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.hidden_size)))
local decLSTM = {}
for layer=1,opt.layers do
  decLSTM[layer] = nn.SeqLSTM(opt.hidden_size, opt.hidden_size)
  dec:add(decLSTM[layer])
end
dec:add(nn.Sequencer(nn.Linear(opt.hidden_size, data_loader:dim())))
dec:remember('neither')

-- Coupler
local ed_coupler = Coupler:new(enc, encLSTM, dec, decLSTM)
local parameters_enc = ed_coupler.encoder:getParameters()
local parameters_dec = ed_coupler.decoder:getParameters()
parameters_enc:uniform(-1, 1)
parameters_dec:uniform(-1, 1)

-- save final model
local enc_cpu = enc:clone(); enc_cpu:double()
local dec_cpu = dec:clone(); dec_cpu:double()
local encLSTM_cpu = {}
local decLSTM_cpu = {}
for layer=1,#encLSTM do
  encLSTM_cpu[layer] = encLSTM[layer]:clone():double()
  decLSTM_cpu[layer] = decLSTM[layer]:clone():double()
end
torch.save(opt.save_file, {encoder = enc_cpu, encoderLSTM = encLSTM_cpu, decoder = dec_cpu, decoderLSTM = decLSTM_cpu, opt = opt})
