package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
local gnuplot = require 'gnuplot'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-n,--network            (default "")              reload pretrained network
-r,--learning_rate      (default 0.001)            learning rate
-t,--threads            (default 4)               number of threads
-g,--gpu                (default -1)              gpu to run on (default cpu)
--epochs                (default 500)             number of epochs to train for
-M,--max_seq_len        (default 1.0)               maximum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-m,--min_seq_len        (default 0.2)               minimum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-b,--batch_size         (default 128)            batch size
--boundaries            (default "")           boundaries file
-o,--out_file            (default "exp.log")       file to write embeddings to
-v,--val_set            (default 0.05)            proportion of data to put in the validation set
]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    print('using CUDA on GPU ' .. opt.gpu .. '...')
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc_dictionary(opt.boundaries, opt.min_seq_len, opt.max_seq_len)
val_data_index = math.floor(opt.val_set*#(data_loader.data))
print(string.format('%d samples, of which %d for validation', #(data_loader.data), val_data_index))

-- load networks
if opt.network and opt.network:len()>0 then
  local saved = torch.load(opt.network)
  ed_coupler = Coupler:new(saved.encoder, saved.encoderLSTM, saved.decoder, saved.decoderLSTM)
end
ed_coupler.encoder:remember('neither')
ed_coupler.decoder:remember('neither')
ed_coupler.encoder:forget()
ed_coupler.decoder:forget()

local parameters_enc, grad_parameters_enc = ed_coupler.encoder:getParameters()
local parameters_dec, grad_parameters_dec = ed_coupler.decoder:getParameters()

local mse_criterion = nn.MSECriterion()
mse_criterion.sizeAverage = false
local mz_criterion = nn.SequencerCriterion(nn.MaskZeroCriterion(mse_criterion,1))
-- hold adadelta state parameters
local enc_state = {}
local dec_state = {}

data_loader:shuffle(1, #(data_loader.data), os.time())
for epoch=1,opt.epochs do
  -- TRAINING
  ed_coupler.encoder:training()
  ed_coupler.decoder:training()
  local epoch_err = 0
  local total_num_frames = 0
  data_loader:shuffle(val_data_index+1, #(data_loader.data), os.time()+epoch)
  for utterance=val_data_index+1,#(data_loader.data),opt.batch_size do
    ed_coupler.encoder:zeroGradParameters()
    ed_coupler.decoder:zeroGradParameters()
    local data, num_frames = data_loader:make_batch(utterance, utterance+opt.batch_size-1)
    -- The input sentences to the encoder. 
    local encInSeq = data:clone() -- seq_len x batch_size x dimension
    math.randomseed(os.time() + utterance + epoch)
    encInSeq = encInSeq + torch.Tensor(encInSeq:size()):normal(0,5)
    if opt.gpu then encInSeq:cuda() end
    -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
    local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
    if opt.gpu then decInSeq:cuda() end
    -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
    local decOutSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then decOutSeq:cuda() end
    -- Forward pass
    ed_coupler:resize_LSTM(torch.zeros(encInSeq:size(1), encInSeq:size(2), ed_coupler.encoderLSTM[1].outputsize))
    local encOut = ed_coupler.encoder:forward(encInSeq)
    ed_coupler:forward_connect(num_frames)
    local decOut = ed_coupler.decoder:forward(decInSeq)
    local err = mz_criterion:forward(decOutSeq, decOut) -- this ignores zero vectors in decOut for err and grad computation. In this implementation we use the fact that in MSECriterion, the error is symmetric in the input and target, and the gradient is an odd function of (input - target). Exchanging target and input allows the masking to take effect.
    for _,length in pairs(num_frames) do total_num_frames = total_num_frames + length end
    epoch_err = epoch_err + err
    -- Backward pass
    local gradOutput = mz_criterion:backward(decOutSeq, decOut):mul(-1)
    ed_coupler.decoder:backward(decInSeq, gradOutput)
    ed_coupler:backwardConnect()
    local zeroTensor = torch.Tensor(encOut):zero()
    ed_coupler.encoder:backward(encInSeq, zeroTensor)
    -- gradient updates have been accumulated in the seq_len and utterance loops
    local feval_enc = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_enc then
        parameters_enc:copy(x)
      end
      grad_parameters_enc:clamp(-1,1)
      return err, grad_parameters_enc
    end
    local feval_dec = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_dec then
        parameters_dec:copy(x)
      end
      grad_parameters_dec:clamp(-1,1)
      return err, grad_parameters_dec
    end
    -- update network parameters
    optim.adadelta(feval_enc, parameters_enc, {}, enc_state) 
    optim.adadelta(feval_dec, parameters_dec, {}, dec_state) -- using default config
  end -- end for utterance
  -- end TRAINING
  ---[[ VALIDATION
  ed_coupler.decoder:evaluate() -- don't switch the encoder to evaluate, otherwise it forgets the embeddings it computed during the sequence, and we lose the information for forward_connect
  local data, num_frames = data_loader:make_batch(1, val_data_index)
  if data then
    -- The input sentences to the encoder. 
    local encInSeq = data:clone() -- seq_len x batch_size x dimension
    encInSeq = encInSeq + torch.Tensor(encInSeq:size()):normal(0,5)
    if opt.gpu then encInSeq:cuda() end
    -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
    local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
    if opt.gpu then decInSeq:cuda() end
    -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
    local decOutSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then decOutSeq:cuda() end
    -- Forward pass
    ed_coupler:resize_LSTM(torch.zeros(encInSeq:size(1), encInSeq:size(2), ed_coupler.encoderLSTM[1].outputsize))
    local encOut = ed_coupler.encoder:forward(encInSeq)
    ed_coupler:forward_connect(num_frames)
    local decOut = ed_coupler.decoder:forward(decInSeq)
    local err = mz_criterion:forward(decOutSeq, decOut) -- this ignores zero vectors in decOut for err and grad computation. In this implementation we use the fact that in MSECriterion, the error is symmetric in the input and target, and the gradient is an odd function of (input - target). Exchanging target and input allows the masking to take effect.
    local total_length = 0
    for _,length in pairs(num_frames) do total_length = total_length + length end
    err = err/total_length -- normalize by actual number of frames
  print(string.format('epoch %d; training_loss %f; validation loss %f', epoch, epoch_err/total_num_frames, err))
  end -- end if data
  -- end VALIDATION ]]
  --xlua.progress(epoch, opt.epochs)
  collectgarbage()
end -- end for epoch
-- write embeddings to file
local embed_file = io.open(opt.out_file, 'w')
for _,tens in pairs(data_loader.data) do
  local embedding = ed_coupler:audio_embedding(tens)
  for ii=1,embedding:size(1) do
    embed_file:write(embedding[ii] .. ' ')
  end -- end for ii
  embed_file:write('\n')
end
embed_file:close()
-- save final model
local enc_cpu = ed_coupler.encoder:clone(); enc_cpu:double()
local dec_cpu = ed_coupler.decoder:clone(); dec_cpu:double()
local encLSTM_cpu = {}
local decLSTM_cpu = {}
for layer=1,#ed_coupler.encoderLSTM do
  encLSTM_cpu[layer] = ed_coupler.encoderLSTM[layer]:clone():double()
  decLSTM_cpu[layer] = ed_coupler.decoderLSTM[layer]:clone():double()
end
torch.save(opt.save_file, {encoder = enc_cpu, encoderLSTM = encLSTM_cpu, decoder = dec_cpu, decoderLSTM = decLSTM_cpu, opt = opt})
