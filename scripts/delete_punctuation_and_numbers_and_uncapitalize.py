import sys
punctuation = ['#','@','&','(','!',')','$','<','>',',','?',';','.',':','/','=','+','*','\'','\n','-','_']
punctuation.extend([str(i) for i in range(10)])
for line in open(sys.argv[1], 'r'):
  for x in punctuation:
    line = line.replace(x,'')
  while '  ' in line:
    line = line.replace('  ', ' ')
  if len(line)>0:
    print(line.lower().strip(' '))
