# !/usr/bin/python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import pickle
import math
import random
import time
import numpy as np
from sklearn import mixture
from sklearn import preprocessing
from scipy import linalg
import sys

# read data
data = list()
single_points = list()
for line in open(sys.argv[1]):
    if len(line) < 2: # empty lines
      continue
    vector_str = line[:-1].strip(' ').split(' ') # assume one vector per line with coefs separated by a blank space
    vector = list()
    # converting to floats
    for string in vector_str:
        vector.append(float(string))
    data.append(vector)
    if vector not in single_points:
      single_points.append(vector) # not easier to use set because a list is not hashable
dimensionality = len(data[0])
random.seed(time.clock())
random.shuffle(data)
# normalize data
scaler = preprocessing.StandardScaler().fit(data)
data_scaled = scaler.transform(data)
# fit GMM
niter = 1024
#estimated_min_covar = 1e-8
estimated_min_covar = 1e-3
#clf = mixture.GMM(n_components=min(int(sys.argv[2]), len(data_scaled)),covariance_type='diag', n_iter=niter, min_covar=estimated_min_covar, verbose=0)
clf = mixture.DPGMM(n_components=min(int(sys.argv[2]), len(data_scaled)),covariance_type='diag', alpha=1.0, n_iter=niter, verbose=0)
clf.fit(data_scaled)
conv = clf.converged_

while not conv:
  niter = niter*2
  clf.fit(data_scaled)
  conv=clf.converged_

# write to file
#pickle.dump(clf, open(sys.argv[2],'w'))
Y = clf.predict(data_scaled)
valid_clusters = list()
for i in range(len(clf.means_)):
# as the DP will not use every component it has access to unless it needs it, we shouldn't plot the redundant components.
    if np.any(Y == i):
        valid_clusters.append(i)
single_points = scaler.transform(single_points)
logprobs, per_cluster = clf.score_samples(single_points) # per_cluster is the prob of each cluster, normalized by total prob (so it sums to 1)
real_logprobs = [math.log(sum([per_cluster[ii][jj] for jj in valid_clusters])) + logprobs[ii] for ii in range(len(logprobs))]
shift = max(real_logprobs) # numerical stability
prob_sum = sum([math.exp(lp - shift) for lp in real_logprobs])

nbr_clusters = len(valid_clusters)
print(str(dimensionality))
print(str(nbr_clusters))
out = ''
for m in scaler.mean_:
    out = out + str(m) + ' '
print(out.strip(' '))
out=''
for s in scaler.scale_:
    out = out + str(s) + ' '
print(out.strip(' '))
# printing the clusters to file
out = ''
for i in valid_clusters: # print out probabilities
    out = out + str(math.log(clf.weights_[i]) - math.log(prob_sum) - shift) + ' '
print(out.strip(' '))
for i in valid_clusters: # mean vectors
    out = ''
    for coef in clf.means_[i]:
        out = out + str(coef) + ' '
    print(out.strip(' '))
for i in valid_clusters:
    out=''
    #for coef in clf.covars_[i]:
        #out = out + str(1.0/coef) + ' '
    for coef in clf.precs_[i]:
        out = out + str(coef) + ' '
    print(out.strip(' '))
