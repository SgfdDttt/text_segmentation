import sys
word_dict = dict()
for line in open(sys.argv[1], 'r'):
  for word in line[:-1].split(' '):
    if word not in word_dict:
      word_dict[word] = 0
    word_dict[word] += 1
def compare_occurrences(word):
  if word in word_dict:
    return word_dict[word]
  else:
    return 0
admissible_words = sorted(word_dict, key=compare_occurrences, reverse=True)[0:int(sys.argv[2])]
for line in open(sys.argv[1], 'r'):
  new = list()
  for word in line[:-1].split(' ')[:-1]:
    if word in admissible_words:
      new.append(word)
  if len(new)>0:
    print(' '.join(new))
