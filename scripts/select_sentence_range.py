import sys
min_len = int(sys.argv[2])
max_len = int(sys.argv[3])
for line in open(sys.argv[1], 'r'):
  if (len(line[:-1]) <= max_len) and (len(line[:-1]) >= min_len):
    print(line[:-1])
