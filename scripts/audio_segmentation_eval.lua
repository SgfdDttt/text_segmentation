local lapp = require 'pl.lapp'
local opt = lapp [[
-i,--input (default "") input file
-g,--gold (default "") gold segmentation
-t,--tolerance (default 0.002) tolerance in seconds
]]

local valid_answer_far_away = function(answer, boundaries)
  local pos = nil
  local best_gap = 0
  for i,b in pairs(boundaries) do 
    if math.abs(answer - b) <= opt.tolerance*(1+1e-3) then -- potential answer. The factor avoids rounding errors when opt.tolerance is just a little larger or equal.
      if pos and best_gap < math.abs(answer - b) then
        pos = i
        best_gap = math.abs(answer - b)
      else
        pos = i
      end
    end
  end -- end for b
  return pos
end

local gold = io.lines(opt.gold)
local input = io.lines(opt.input)
local gold_seg = gold()
local inp_seg = input()
local correct_answers = 0
local expected_correct_answers = 0
local total_answers = 0
while gold_seg and inp_seg do -- score line by line
  local gold_bound = {}
  local inp_bound = {}
  for b in gold_seg:gmatch('%S+') do -- put boundaries in table
    table.insert(gold_bound, b)
  end
  for b in inp_seg:gmatch('%S+') do
    table.insert(inp_bound, b)
  end
  expected_correct_answers = expected_correct_answers + #gold_bound
  total_answers = total_answers + #inp_bound
  for _,answer in pairs(inp_bound) do -- greedily match boundaries
   local pos = valid_answer_far_away(answer, gold_bound) 
   if pos then 
     correct_answers = correct_answers + 1
     table.remove(gold_bound, pos)
   end
  end
  gold_seg = gold() -- get next lines
  inp_seg = input()
end -- end while gold_seg and inp_seg

print('fscore, precision, recall (correct answers, total answers, expected correct answers')
print(string.format('%f %f %f (%d %d %d)', 2*correct_answers/(total_answers + expected_correct_answers), correct_answers/total_answers, correct_answers/expected_correct_answers, correct_answers, total_answers, expected_correct_answers)) -- fscore, precision, recall
