import sys
word_lengths = dict()
for line in open(sys.argv[1], 'r'):
  words = line[:-1].split(' ')
  for word in words:
    length = len(word)
    if length not in word_lengths:
      word_lengths[length] = 0
    word_lengths[length] += 1
cumulated_fraction = dict()
acc = 0
for length in sorted(word_lengths):
  acc += word_lengths[length]
  cumulated_fraction[length] = acc
for length in sorted(word_lengths):
  print(str(length) + ' -> ' + str(word_lengths[length]) + ' ; ' + str(float(cumulated_fraction[length])/float(acc)*100) + '%')
