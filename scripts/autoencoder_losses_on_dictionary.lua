Coupler = require '../../rnn_autoencoder/encoder_decoder_coupler'
require 'nn'
require 'rnn'
pcall(require,'../../rnn/SeqLSTM')

opt = lapp[[
-n,--network   (default "")   network to test: load a cpu net
-d,--dictionary    (default "") dictionary to compute losses on
-g,--gpu           (default -1) gpu
--]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

-- load saved state
save_state = torch.load(opt.network)

-- load trained encoder and decoder')
local criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
local coupler = Coupler:new(save_state.encoder, save_state.encoderLSTM, save_state.decoder, save_state.decoderLSTM)
if opt.gpu then
  coupler:cuda()
end

-- put dictionary into table
local dictionary = {} -- all words from input file
local length_counts = {} -- length_counts[i] is how many words of length[i] there are. For plotting purposes
for line in io.lines(opt.dictionary) do
  local word = line:gsub('\n','') -- there should only be one per line
  word = word:gsub(' ','')
  word = word:gsub('\t','')
  dictionary[#dictionary+1] = word
end -- end for line

-- load character maps
local int_to_char = save_state.int_to_char
local char_to_int = save_state.char_to_int

-- convert dictionary words to sequences of indices
string_to_sequence = {}
for _,string in pairs(dictionary) do
  sequence = {}
  for c in string:gmatch('.') do
    sequence[#sequence+1] = char_to_int[c]
  end -- end for c
  string_to_sequence[string] = sequence
end -- end for string,prob

-- compute autoencoder negative log likelihood losses
local sorted_strings = {}
for string, _ in pairs(string_to_sequence) do
  sorted_strings[#sorted_strings+1]=string
end
table.sort(sorted_strings)
local string_to_losses = {}
for jj,string in pairs(sorted_strings) do
  local sequence = string_to_sequence[string]
  -- The input sentences to the encoder. 
  local encInSeq = torch.Tensor({ sequence }):t(1,2)
  if opt.gpu then encInSeq:cuda() end
  -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
  for ii=#sequence,1,-1 do
    sequence[ii+1] = sequence[ii]
  end
  sequence[1] = char_to_int['<s>']
  local decInSeq = torch.Tensor({ sequence }):t(1,2)
  if opt.gpu then decInSeq:cuda() end
  -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
  for ii=2,#sequence do
    sequence[ii-1] = sequence[ii]
  end
  sequence[#sequence] = char_to_int['<eos>']
  local decOutSeq = torch.Tensor({ sequence })
  if opt.gpu then decOutSeq:cuda() end
  -- The decoder predicts one per timestep, so we split accordingly.
  decOutSeq = nn.SplitTable(1, 1):forward(decOutSeq)

  -- Forward pass
  local encOut = coupler.encoder:forward(encInSeq)
  coupler:forwardConnect(seq_len)
  local decOut = coupler.decoder:forward(decInSeq)
  local loss = criterion:forward(decOut, decOutSeq)

  -- print out loss
  loss = -loss/#decOutSeq -- use actual logprob, normalize by length
  io.write(string .. ' ' .. loss .. '\n')
end -- end for string, sequence
