import sys
line_lengths = dict()
for line in open(sys.argv[1], 'r'):
  length = len(line[:-1])
  if length not in line_lengths:
    line_lengths[length] = 0
  line_lengths[length] += 1
cumulated_fraction = dict()
acc = 0
for length in sorted(line_lengths):
  acc += line_lengths[length]
  cumulated_fraction[length] = acc
for length in sorted(line_lengths):
  print(str(length) + ' -> ' + str(line_lengths[length]) + ' ; ' + str(float(cumulated_fraction[length])/float(acc)*100) + '%')
