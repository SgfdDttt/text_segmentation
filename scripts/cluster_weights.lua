package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-n,--network            (default "")              reload pretrained network
-b,--batch_size         (default 128)            batch size
--boundaries            (default "")           boundaries file
-M,--max_seq_len   (default 1.0)       maximum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-m,--min_seq_len  (default 0.2)     minimum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
]]

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc_dictionary(opt.boundaries, opt.min_seq_len, opt.max_seq_len)

-- Load networks
local saved = torch.load(opt.network)

-- Coupler
local ed_coupler = Coupler:new(saved.encoder, saved.encoderLSTM, saved.decoder, saved.decoderLSTM, saved.ngmm)

-- count embeddings belonging to each cluster
local cluster_counts = {}
local num_clusters = 0

-- compute posteriorgrams
ed_coupler.encoder:training()
for utterance=1,#(data_loader.data),opt.batch_size do
  ed_coupler.encoder:zeroGradParameters()
  ed_coupler.decoder:zeroGradParameters()
  local data, num_frames = data_loader:make_batch(utterance, utterance+opt.batch_size-1)
  -- The input sentences to the encoder. 
  local encInSeq = data:clone() -- seq_len x batch_size x dimension
  -- Forward pass
  local posteriorgrams = ed_coupler:embeddings(data, num_frames) -- batch_size x num_clusters
  --print(posteriorgrams)
  num_clusters = posteriorgrams:size(2)
  local _,max_ind = torch.max(posteriorgrams, 2) -- batch size
  for ind=1,max_ind:size(1) do
    local cluster = max_ind[ind][1]
    cluster_counts[cluster] = (cluster_counts[cluster] or 0) + 1
  end -- end for cluster
end -- end for utterance

--print(cluster_counts)
-- print posteriorgrams
total_count=0
for ii=1,num_clusters do
total_count=total_count + (cluster_counts[ii] or 0)
end
--print(total_count)
for ii=1,num_clusters do
  io.write((cluster_counts[ii] or 0)/total_count .. ' ')
end
