import sys
import random
# start is assumed to be 1
random.seed()
increment=int(sys.argv[1])
stop=int(sys.argv[2])
arr = list()
for i in range(1, stop+1, increment):
    arr.append(str(i))
random.shuffle(arr)
print(' '.join(arr))
