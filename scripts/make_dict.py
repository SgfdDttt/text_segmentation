import sys
word_dict = set()
for line in open(sys.argv[1], 'r'):
  line = line.replace('\n','').strip(' ')
  word_dict = word_dict.union(set(line.split(' ')))
dict_sorted = list(word_dict)
dict_sorted.sort()
for word in dict_sorted:
  print(word)
