import sys
old_dict=dict()
for line in open(sys.argv[1], 'r'):
  word_and_count=line[:-1].split(" ")
  old_dict[word_and_count[0]]=word_and_count[1]
old_words = set(old_dict.keys())
new_words = set()
for line in open(sys.argv[2], 'r'):
  new_words.add(line[:-1].split(" ")[0])
neg_words = list(old_words.difference(new_words))
neg_words.sort()
for word in neg_words:
  print(word + " " + old_dict[word])
