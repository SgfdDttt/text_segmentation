# !/usr/bin/python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import pickle
import math
import random
import numpy as np
from sklearn import mixture
from sklearn import preprocessing
from scipy import linalg
import sys

# read data
data = list()
for line in open(sys.argv[1]):
    if len(line) < 2: # empty lines
      continue
    vector_str = line[:-1].split(' ') # assume one vector per line with coefs separated by a blank space
    vector = list()
    # converting to floats
    for string in vector_str:
        vector.append(float(string))
    data.append(vector)
random.shuffle(data)
dimensionality = len(data[0])
# normalize data
scaler = preprocessing.StandardScaler().fit(data)
data_scaled = scaler.transform(data)
# fit GMM
niter = 128
alpha_expected = 2/math.log(2007)
alpha_expected = 1
clf = mixture.DPGMM(n_components=int(sys.argv[2]),covariance_type='diag', alpha=alpha_expected, n_iter=niter, verbose=0)
clf.fit(data_scaled)
conv = clf.converged_

while not conv:
  niter = niter*2
  clf.fit(data_scaled)
  conv=clf.converged_

# write to file
# pickle.dump(clf, open(sys.argv[2],'w'))
Y = clf.predict(data_scaled)
valid_clusters = list()
for i in range(len(clf.means_)):
    # as the DP will not use every component it has access to unless it needs it, we shouldn't plot the redundant components.
    if np.any(Y == i):
        valid_clusters.append(i) 

nbr_clusters = len(valid_clusters)
print(str(dimensionality))
print(str(nbr_clusters))
out = ''
for m in scaler.mean_:
  out = out + str(m) + ' '
print(out.strip(' '))
out=''
for s in scaler.scale_:
  out = out + str(s) + ' '
print(out.strip(' '))
# printing the clusters to file
out = ''
for i in valid_clusters: # print out probabilities
  out = out + str(clf.weights_[i]) + ' '
print(out.strip(' '))
for i in valid_clusters: # mean vectors
  out = ''
  for coef in clf.means_[i]:
    out = out + str(coef) + ' '
  print(out.strip(' '))
for i in valid_clusters:
  out=''
  #for line in c: # these matrices are not symmetric. so hopefully it's line in c and not column in c
  for coef in clf.precs_[i]:
    out = out + str(coef) + ' '
  print(out.strip(' '))

