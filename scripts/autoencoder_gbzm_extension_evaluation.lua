local gnuplot = require 'gnuplot'
Coupler = require '../../rnn_autoencoder/encoder_decoder_coupler'
require 'nn'
require 'rnn'

opt = lapp[[
-n,--network   (default "")   network to test: load a cpu net
-c,--corpus    (default "") text corpus to test autoencoder against
--]]

print('load saved state')
print(opt.network)
save_state = torch.load(opt.network)

print('load trained encoder and decoder')
if save_state.encoder then print('encoder ok') else print('problem with encoder') end
if save_state.encoderLSTM then print('encoderLSTM ok') else print('problem with encoderLSTM') end
if save_state.decoder then print('decoder ok') else print('problem with decoder') end
if save_state.decoderLSTM then print('decoderLSTM ok') else print('problem with decoderLSTM') end
local criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
local coupler = Coupler:new(save_state.encoder, save_state.encoderLSTM, save_state.decoder, save_state.decoderLSTM)

print('load char maps')
local int_to_char = save_state.int_to_char
local char_to_int = save_state.char_to_int

print('extract dictionary from corpus')
local appearance_probs = {} -- measured in synthetic text
local words = {'g', 'bu', 'zol', 'meuk'} -- real words
local full = {} -- all letter combinations
local word
for ii=1,9 do -- to avoid <s> and <eos> tokens
  for jj=1,10 do
    for ll=1,10 do
      for aa=1,10 do
        table.insert(full, int_to_char[ii] .. (jj<10 and int_to_char[jj] or "") .. (aa<10 and int_to_char[aa] or "") .. (ll<10 and int_to_char[ll] or ""))
      end -- end for aa
    end -- end for ll
  end -- end for jj
end -- end for ii
--print(full)
local length_counts = {} -- length_counts[i] is how many words of length[i] there are. For plotting purposes
local max_len = 0
local total_counts = 0
for line in io.lines(opt.corpus) do
  for n=0,3 do
    for pos=1,line:len()-n do
      word=line:sub(pos,pos+n)
      appearance_probs[word] = (appearance_probs[word] or 0) + 1
      length_counts[word:len()] = (length_counts[word:len()] or 0) + 1
      max_len = math.max(max_len, word:len())
      total_counts = total_counts + 1
    end -- end for pos
  end -- end for n
end -- end for line
local legal_comb = {} -- legal, non-word letter combinations
for word in pairs(appearance_probs) do
  if (word ~= 'g') and (word ~= 'bu') and (word ~= 'zol') and (word ~= 'meuk') then
    table.insert(legal_comb, word)
  end
end -- end for word
local illegal = {} -- letter combinations that never occur in the text
for _,word in pairs(full) do
  if not appearance_probs[word] then
    illegal[#illegal+1] = word
  end
end -- end for word
print('convert dictionary words to sequences of indices')
string_to_sequence = {}
for _,string in pairs(full) do
  sequence = {}
  for c in string:gmatch('.') do
    sequence[#sequence+1] = char_to_int[c]
  end -- end for c
  string_to_sequence[string] = sequence
end -- end for string,prob
print('compute autoencoder negative log likelihood losses')
local losses = {}
for string, sequence in pairs(string_to_sequence) do
  -- The input sentences to the encoder. 
  --print(string)
  --print(sequence)
  local encInSeq = torch.Tensor({ sequence }):t(1,2)
  if opt.gpu then encInSeq:cuda() end
  -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
  local seq2 = {}
  for ii=1,#sequence do
    seq2[ii+1] = sequence[ii]
  end
  seq2[1] = char_to_int['<s>']
  local decInSeq = torch.Tensor({ seq2 }):t(1,2)
  if opt.gpu then decInSeq:cuda() end
  -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
  local seq2 = {}
  for ii=1,#sequence do
    seq2[ii] = sequence[ii]
  end
  seq2[#seq2+1] = char_to_int['<eos>']
  -- The decoder predicts one per timestep, so we split accordingly.
  local decOutSeq = nn.SplitTable(1, 1):forward(torch.Tensor({ seq2 }))
  if opt.gpu then decOutSeq:cuda() end

  -- Forward pass
  local encOut = coupler.encoder:forward(encInSeq)
  coupler:forwardConnect()
  local decOut = coupler.decoder:forward(decInSeq)
  local loss = criterion:forward(decOut, decOutSeq)
  losses[string]=loss
 -- losses[string]=loss/#decOutSeq
end -- end for string, sequence

-- put data into plotable tensor
local plots = {}
-- gather all the data for linear regression
local x_all = {}
local y_all = {}
-- words
local x = {}
local y = {}
local ind = 1 
for _,string in pairs(words) do
  x[ind] = -math.log(appearance_probs[string]) + math.log(total_counts)
  y[ind] = losses[string]
  ind = ind + 1
end -- end for ind,string
for index in pairs(x) do table.insert(x_all, x[index]); table.insert(y_all, y[index]) end
if (#x>0) and (#y>0) then
  plots[1] = {string.format('words'), torch.Tensor(x):clone(), torch.Tensor(y):clone(), '+'}
end -- end if #x...
-- possible non-words
local x = {}
local y = {}
local ind = 1 
for _,string in pairs(legal_comb) do
  x[ind] = -math.log(appearance_probs[string]) + math.log(total_counts)
  y[ind] = losses[string]
  ind = ind + 1
end -- end for ind,string
for index in pairs(x) do table.insert(x_all, x[index]); table.insert(y_all, y[index]) end
if (#x>0) and (#y>0) then
  plots[2] = {string.format('possible non-words'), torch.Tensor(x):clone(), torch.Tensor(y):clone(), '+'}
end -- end if #x...
-- impossible non-words
local x = {}
local y = {}
local ind = 1 
for _,string in pairs(illegal) do
  for c in string:gmatch('.') do
    x[ind] = (x[ind] or 0) - math.log(appearance_probs[c]) + math.log(total_counts)
  end
  x[ind] = x[ind]
  y[ind] = losses[string]
  ind = ind + 1
end -- end for ind,string
--for index in pairs(x) do table.insert(x_all, x[index]); table.insert(y_all, y[index]) end
if (#x>0) and (#y>0) then
  plots[3] = {string.format('impossible non-words'), torch.Tensor(x):clone(), torch.Tensor(y):clone(), '+'}
end -- end if #x...
if (#x_all>0) and (#y_all==#x_all) then
  linear_regression = {}
  local x_tens,y_tens = torch.Tensor(x), torch.Tensor(y)
  local xy = torch.cmul(x_tens,y_tens):sum()
  local xx, yy = torch.cmul(x_tens,x_tens):sum(), torch.cmul(y_tens,y_tens):sum()
  local x_mean, y_mean = x_tens:mean(), y_tens:mean()
  local n = x_tens:size(1)
  linear_regression[2] = (xy - n*x_mean*y_mean) / (xx - n*x_mean*x_mean) -- beta
  linear_regression[1] = y_mean - linear_regression[2] * x_mean -- alpha
  linear_regression[3] = (xy - n*x_mean*y_mean) / math.sqrt((xx - n*x_mean*x_mean)*(yy - n*y_mean*y_mean)) -- r
  print(string.format('slope %f, intercept %f, residuals %f', linear_regression[2], linear_regression[1], linear_regression[3]))
end -- end if #x...
-- plot
torch.setdefaulttensortype('torch.FloatTensor')
gnuplot.title(opt.network)
gnuplot.xlabel('negative log frequency of appearance')
gnuplot.ylabel('reconstruction loss of autoencoder')
gnuplot.movelegend('left','top')
gnuplot.plot(unpack(plots))
--gnuplot.plot(plots[1])
