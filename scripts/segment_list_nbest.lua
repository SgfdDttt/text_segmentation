opt=lapp[[
-d,--dictionary   (default "")   file containing the losses
-N,--N            (default 5)    N for N best list
-i,--input         (default "")     file to parse
-t,--temperature   (default 1.0)    temperature for parsing
-M,--max_len       (default 20)    a prior on the max length of a word, also used to avoid out of mem errors
-K,--constant      (default 3)
-o,--output        (default "")    redirect parse output to file
]]

if opt.input:len()==0 then print('no input specified') end
if opt.output:len()==0 then opt.output=nil end

local read_dictionary = function(string)
  local out = {}
  local iter = string:gmatch('%S+')
  local word = iter()
  local count = iter()
  return word,count
end

local split_string = function(string, index)
  return {string:sub(1,index), string:sub(index+1)}
end

local logprob = function(word)
  local epsilon = 1.5
  --local out = math.log(10)*(opt.constant - word:len()) -- TODO find a better penalty.
  --local out = -math.huge -- TODO find a better penalty.
  local out = 0
  if count_dictionary[word] then
    out = math.log(count_dictionary[word]) - log_total_count
  else
    for char in word:gmatch('.') do
      out = out + math.log(letter_counts[char]) - log_total_letter_count -- epsilon
    end -- end for char
  end
  return out
end

local parse_log_likelihood
parse_log_likelihood = function(word_list)
  local out = 0
  if llk_cache[word_list] then
    out = llk_cache[word_list]
  else
    if #word_list == 1 then
      out = logprob(word_list[1]) 
    elseif #word_list > 1 then
      local first_half = {}
      local second_half = {}
      -- anything nicer to split an array in half?
      for ii=1,math.floor(#word_list/2) do
        first_half[#first_half+1] = word_list[ii]
      end
      for ii=math.floor(#word_list/2)+1,#word_list do
        second_half[#second_half+1] = word_list[ii]
      end
      out = parse_log_likelihood(first_half) + parse_log_likelihood(second_half)
    end -- end if
    llk_cache[word_list] = out
  end -- end if llk_cache[word_list]
  return out
end

local nbest_unsorted = function(parse_list, n) -- this can change the order of parse_list but won't destroy it
  local max_len = #parse_list -- parse list is a list of lists of strings. Each item is a parse
  local out = {}
  repeat
    local maxi = 1
    for ii=1,max_len do
      maxi = ((parse_log_likelihood(parse_list[ii]) > parse_log_likelihood(parse_list[maxi])) and ii) or maxi
    end
    out[#out+1] = parse_list[maxi]
    parse_list[maxi], parse_list[max_len] = parse_list[max_len], parse_list[maxi] -- exchange both parses
    max_len = max_len - 1
  until #out == math.min(n, #parse_list)
  return out
end

local nbest_sorted = function(parse_list, n, min_llk)
  -- parse list is a list of lists of strings, sorted by likelihood of parse (1 is best)
  min_llk = min_llk or -math.huge
  local new_min_llk = -math.huge
  local out = {}
  for ii=1,#parse_list do
    if (parse_log_likelihood(parse_list[ii]) >= min_llk) then -- only add if llk high enough
      table.insert(out, parse_list[ii])
      new_min_llk = parse_log_likelihood(parse_list[ii]) -- because parse_list is sorted
    else
      break -- because parse_list is sorted
    end -- end if
    if #out == n then break end
  end -- end for ii
  return out, new_min_llk
end

--[[ this function is slower than inser_into_sorted in practice, and the lists used are tipycally short (< 10) so we don't use it
local insert_into_sorted_dichotomic = function(tab, element) -- tab is sorted with best element at index 1
  if #tab == 0 then
    tab[1] = element
  else
    local upper = #tab + 1
    local lower = 0
    local elem_likelihood = parse_log_likelihood(element)
    repeat
      local pivot = math.floor((lower + upper)/2)
      if elem_likelihood > parse_log_likelihood(tab[pivot]) then
        upper = pivot
      else
        lower = pivot
      end -- end if 
    until upper - lower < 2 -- end while lower ~= upper
    table.insert(tab, upper, element)
  end -- end if #tab ...
end
--]]

local insert_into_sorted = function(tab, element) -- tab is sorted with best element at index 1
  if #tab == 0 then
    tab[1] = element
  else
    local elem_likelihood = parse_log_likelihood(element)
    local index = #tab
    while (index > 0) and (elem_likelihood > parse_log_likelihood(tab[index])) do
      index = index - 1
    end -- end while index > 0 ...
    index = index + 1 -- this while loop goes one index too far up
    table.insert(tab, index, element)
  end -- end if #tab ...
end

local test_if_sorted = function(tab) -- for debugging purposes
  sorted = true
  for ii=2,#tab do
    sorted = sorted and (parse_log_likelihood(tab[ii]) < parse_log_likelihood(tab[ii-1]))
  end
  return sorted
end

local parse_string_fast
parse_string_fast = function(string)
  local all_parses = {}
  if parse_cache[string] then
    all_parses = parse_cache[string]
  else
    if string:len() == 0 then
      all_parses[1] = {}
    else
      local min_llk = -math.huge -- keep in mind the best parses to avoid parsing some stuff
      for k=1,math.min(string:len(),opt.max_len) do
        local strings = split_string(string,k)
        if logprob(strings[1]) < min_llk then break end
        -- explanation: all the following are parses with the first cut after character at index k. Thus the llk of these parses can only be lower than the log prob of word s_1...s_k. So if logprob(s_1...s_k) is already too low no need to bother computing the parses of s_{k+1}...s_n
        --local best_here = nbest_sorted(parse_string_fast(strings[2]), opt.N, min_llk - logprob(strings[1]))
        local best_here = nbest_sorted(parse_string_fast(strings[2]), opt.N, min_llk)
        for i=1,math.min(opt.N,#best_here) do -- maybe 4N or N/2
          insert_into_sorted(all_parses, {strings[1], unpack(best_here[i])})
          all_parses[opt.N + 1] = nil -- remove elements with too low log likelihood
          --if not test_if_sorted(all_parses) then print('PROBLEM') end
        end -- end for i
        min_llk = (all_parses[opt.N] and parse_log_likelihood(all_parses[opt.N])) or -math.huge 
      end -- end for k
    end -- end if
    parse_cache[string] = all_parses
  end -- end if
  return all_parses
end

local softmax = function(parses, temperature) -- simplify and stabilize sampling
  -- parses is short so no need to optimize this
  temperature = temperature or 1.0
  local result = {}
  for ii,parse in pairs(parses) do -- collect log likelihoods (limit calls to parse_log_likelihood)
    result[ii] = parse_log_likelihood(parse)
  end
  local mean = 0
  local denom = 0
  for _,lkh in pairs(result) do -- compute mean log likelihood
    mean = mean + lkh
  end
  mean = mean / #parses
  for ii,lkh in pairs(result) do -- subtract mean for stability, divide by temperature, and exponentiate
    result[ii] = math.exp((lkh-mean)/temperature)
    denom = denom + result[ii]
  end
  for ii,lkh in pairs(result) do -- normalize
    result[ii] = result[ii] / denom
  end
  return result
end

local sample_parse = function(parses, random_seed, temperature)
  if random_seed then math.randomseed(random_seed) end
  temperature = temperature or 0.0 -- between > 0.01 (otherwise unstable numerical results) and +inf, modulates the probabilities. Low temperature -> most likely parse is selected, high temperature -> all parses equally likely.
  local resi = 0
  if temperature > 0.0 then
    local probs = softmax(parses, temperature)
    local random = math.random()
    repeat
      resi = resi + 1
      random = random - probs[resi]
    until (random < 0) or (resi == #probs)
  else
    local probs = softmax(parses, 1.0)
    resi = 1
    for ii in pairs(probs) do
      resi = ((probs[ii] > probs[resi]) and ii) or resi
    end -- end for i,prob
  end -- end if temperature > 0.0
  return parses[resi]
end

-- global variables
count_dictionary = {} -- contains counts for each string, storing them as we go or all at once
letter_counts = {} -- contains counts for each letter, used as backoff
llk_cache = {} -- maps parses to their llk
parse_cache = {} -- maps strings to nbest parses

total_count = 0
total_letter_count = 0
for line in io.lines(opt.dictionary) do
  word,count = read_dictionary(line)
  count_dictionary[word] = count + 0.0
  total_count = total_count + count
  for char in word:gmatch('.') do
    letter_counts[char] = (letter_counts[char] or 0) + count
    total_letter_count = total_letter_count + count
  end -- end for char
end -- end for line
total_count = math.max(1,total_count)
total_letter_count = math.max(1,total_letter_count)
log_total_count = math.log(total_count)
log_total_letter_count = math.log(total_letter_count)

--local time = os.time()
local out_file
if opt.output then
  out_file = assert(io.open(opt.output, 'w'))
end
for line in io.lines(opt.input) do
  local ok,parses = pcall(parse_string_fast, line)
  local out = line .. '\n'
  --local parse = parses[1] -- print just the best parse
  if ok and (#parses > 0) then
    local parse = sample_parse(parses, os.time(), opt.temperature) -- sample the final parse
    --[[
    for key in pairs(parses) do
    print(parses[key])
    print(parse_log_likelihood(parses[key]))
    end
    --]]
    out = table.concat(parse, " ") .. '\n'
  end -- end if
    if opt.output then
      out_file:write(out);
      out_file:flush()
    else
      io.write(out);
      io.flush()
    end
    llk_cache = {}
    parse_cache = {} -- avoid running out of memory
end -- end for line
letter_counts = nil
count_dictionary = nil
llk_cache = nil
parse_cache = nil -- just in case it could be called from the same script with the values filled in by a previous call...
--time = os.time() - time
