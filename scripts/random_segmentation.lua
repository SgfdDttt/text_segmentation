package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local gnuplot = require 'gnuplot'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-M,--max_seq_len        (default 1.0)               maximum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-m,--min_seq_len        (default 0.2)               minimum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
--boundaries            (default "")           boundaries file
]]

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc(opt.directory, opt.list)

bound_file = assert(io.open(opt.boundaries, 'w'))
for utterance=1,#(data_loader.data) do
  local utt = data_loader.data[utterance] -- seq_len x dimension
  local length = utt:size(1) / data_loader.sampling_rates[utterance] -- time length of the utterance
  local upper_bound_num_segments = math.ceil(length / opt.min_seq_len)
  local random_boundaries = torch.Tensor(upper_bound_num_segments) -- contains distances between boundaries
  random_boundaries:normal((opt.min_seq_len+opt.max_seq_len)/2, (opt.max_seq_len-opt.min_seq_len)/2)
  random_boundaries:clamp(opt.min_seq_len, opt.max_seq_len)
  local acc = 0
  for ii=1,random_boundaries:size(1) do
    bound_file:write(acc .. ' ')
    acc = acc + random_boundaries[ii]
    acc = length+1
    if acc > length then bound_file:write(length); break end
  end -- end for ii
  bound_file:write('\n')
end -- end for utterance
bound_file:close()
