import sys
ngram_total = set()
min_ngram_size = int(sys.argv[2])
max_ngram_size = int(sys.argv[3])
for line in open(sys.argv[1], 'r'):
  line = line.replace('\n','')
  for n in range(min_ngram_size, max_ngram_size+1):
    ngrams = [line[i:i+n] for i in range(len(line)-n+1)]
    for ngram in ngrams:
      if ngram not in ngram_total:
        ngram_total.add(ngram)
ngram_sorted=list(ngram_total)
ngram_sorted.sort()
for ngram in ngram_sorted:
  print(ngram)
