package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
local gnuplot = require 'gnuplot'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
--boundaries            (default "") audio boundaries within files
-r,--learning_rate      (default 0.001)            learning rate
-g,--gpu                (default -1)              gpu to run on (default cpu)
-D,--dec_size           (default 128)          num of units in decoder
-E,--enc_size            (default 128)          num of units in encoder
-l,--layers             (default 2)               number of layers for both nets
--epochs                (default 500)             number of epochs to train for
-M,--max_seq_len        (default 1.0)               maximum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-m,--min_seq_len        (default 0.2)               minimum length of speech snippet in seconds. Default taken from Kamper, Jansen, Goldwater.
-b,--batch_size         (default 128)            batch size
-v,--val_set            (default 0.05)            proportion of data to put in the validation set
-p,--plot    (default 1)      plot training loss etc
-K,--num_clusters       (default 8)     number of clusters for weighted euclidean
]]

--[[
local kl_loss = function(input, target)
  -- input is batch_size x featsize
  -- target is featsize and represents a probability distribution
  local mean_activations = torch.mean(input, 1) -- featsize
  local loss = 0
  for ii=1,target:size(1) do -- this could certainly be accelerated by using torch's functions
    local x,t = mean_activations[1][ii],target[ii]
    loss = loss + x*math.log(x/t) + (1-x)*math.log((1-x)/(1-t))
  end -- end for ii
  return loss*input:size(1) -- because loss is already a mean over the batch
end

local kl_grad = function(input, target)
  -- input is batch_size x featsize
  -- target is featsize and represents a probability distribution
  local mean_activations = torch.mean(input, 1) -- featsize
  local gradient = torch.zeros(target:size())
  local loss = 0
  for ii=1,target:size(1) do -- this could certainly be accelerated by using torch's functions
    local x,t = mean_activations[1][ii],target[ii]
    gradient[ii] = math.log(x/t) - math.log((1-x)/(1-t))
  end -- end for ii
  return torch.repeatTensor(gradient,input:size(1),1) -- each sample needs a gradient
end
--]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    print('using CUDA on GPU ' .. opt.gpu .. '...')
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

-- open log files

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc_dictionary(opt.boundaries, opt.min_seq_len, opt.max_seq_len)
print('dimension: ' .. data_loader:dim())
val_data_index = math.floor(opt.val_set*#(data_loader.data))
print(string.format('%d samples, of which %d for validation', #(data_loader.data), val_data_index))

-- data holders to plot training and validation losses
local training_loss, validation_loss, l1_loss = {}, {}, {}
local dec_gradient, enc_gradient, ngmm_gradient = {}, {}, {}

-- Encoder
local enc = nn.Sequential()
enc:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.enc_size)))
local encLSTM = {}
for layer=1,opt.layers do
  encLSTM[layer] = nn.SeqLSTM(opt.enc_size, opt.enc_size)
  enc:add(encLSTM[layer])
end
enc:remember('neither')

-- Transfer layer (NGMM)
local ngmm = nn.Sequential()
--[[
ngmm:add(nn.WeightedEuclidean(opt.enc_size, opt.num_clusters)) -- here are the means and covariance matrices
ngmm:add(nn.Power(2))
ngmm:add(nn.MulConstant(-1))
ngmm:add(nn.Exp()) -- these are the GMM probabilities
ngmm:add(nn.Linear(opt.num_clusters, opt.dec_size)) -- transfer between number of clusters and hidden state of decoder
--]]
ngmm:add(nn.Linear(opt.enc_size, opt.num_clusters))
ngmm:add(nn.Sigmoid())
ngmm:add(nn.Linear(opt.num_clusters, opt.dec_size))

-- Decoder
local dec = nn.Sequential()
dec:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.dec_size)))
local decLSTM = {}
for layer=1,opt.layers do
  decLSTM[layer] = nn.SeqLSTM(opt.dec_size, opt.dec_size)
  dec:add(decLSTM[layer])
end
dec:add(nn.Sequencer(nn.Linear(opt.dec_size, data_loader:dim())))
dec:remember('neither')

-- Coupler
local ed_coupler = Coupler:new(enc, encLSTM, dec, decLSTM, ngmm)
if opt.gpu then -- ship networks to GPU
  ed_coupler:cuda()
end
local parameters_enc, grad_parameters_enc = ed_coupler.encoder:getParameters()
local parameters_dec, grad_parameters_dec = ed_coupler.decoder:getParameters()
local parameters_ngmm, grad_parameters_ngmm = ed_coupler.ngmm:getParameters()
parameters_enc:uniform(-0.01, 0.01)
parameters_ngmm:uniform(-0.01, 0.01)
parameters_dec:uniform(-0.01, 0.01)

--[[
print('encoder')
print(ed_coupler.encoder)
print('ngmm')
print(ed_coupler.ngmm)
print('decoder')
print(ed_coupler.decoder)
--]]

local mse_criterion = nn.MSECriterion()
mse_criterion.sizeAverage = false
local mz_criterion = nn.SequencerCriterion(nn.MaskZeroCriterion(mse_criterion,1))
local zipf_distribution = torch.Tensor(opt.num_clusters):fill(0)
local alpha = math.log(2)
for ii=1,zipf_distribution:size(1) do zipf_distribution[ii] = -alpha*ii end
--for ii=1,zipf_distribution:size(1) do zipf_distribution[ii] = 1 end
zipf_distribution = nn.SoftMax():forward(zipf_distribution)
zipf_distribution[1], zipf_distribution[2] = 34/47, 13/47
print(zipf_distribution)
-- hold adadelta state parameters
local enc_state, dec_state, ngmm_state = {}, {}, {}

for epoch=1,opt.epochs do
  -- TRAINING
  ed_coupler.encoder:training()
  ed_coupler.decoder:training()
  local epoch_err,l1_err = 0,0
  local total_num_frames = 0
  data_loader:shuffle(val_data_index+1, #(data_loader.data), os.time()+epoch)
  for utterance=val_data_index+1,#(data_loader.data),opt.batch_size do
    ed_coupler.encoder:zeroGradParameters()
    ed_coupler.ngmm:zeroGradParameters()
    ed_coupler.decoder:zeroGradParameters()
    local data, num_frames = data_loader:make_batch(utterance, utterance+opt.batch_size-1)
    -- The input sentences to the encoder. 
    local encInSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then encInSeq:cuda() end
    -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
    local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
    if opt.gpu then decInSeq:cuda() end
    -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
    local decOutSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then decOutSeq:cuda() end
    -- Forward pass
    local encOut = ed_coupler.encoder:forward(encInSeq)
    local unmaskedEncOut = torch.zeros(encOut:size(2), encOut:size(3)) -- batch_size x dimension
    for input,last_index in pairs(num_frames) do unmaskedEncOut[input] = encOut[last_index][input] end
    ed_coupler.ngmm:forward(unmaskedEncOut)
    --print(ed_coupler.ngmm.modules[#ed_coupler.ngmm.modules-1].output)
    ed_coupler:ngmm_to_dec_forward_connect()
    local decOut = ed_coupler.decoder:forward(decInSeq)
    local err = mz_criterion:forward(decOutSeq, decOut) -- this ignores zero vectors in decOut for err and grad computation. In this implementation we use the fact that in MSECriterion, the error is symmetric in the input and target, and the gradient is an odd function of (input - target). Exchanging target and input allows the masking to take effect.
    for _,length in pairs(num_frames) do total_num_frames = total_num_frames + length end
    epoch_err = epoch_err + err
     -- Backward pass
    local gradOutput = mz_criterion:backward(decOutSeq, decOut):mul(-1)
    ed_coupler.decoder:backward(decInSeq, gradOutput)
    local decGradOut = ed_coupler:dec_to_ngmm_backward_connect()
    local cheat_coef = 0 --1000000
    l1_err = l1_err + cheat_coef*Coupler:kl_loss(ed_coupler.ngmm.modules[2].output, zipf_distribution)
    local ngmm_reg = cheat_coef*Coupler:kl_grad(ed_coupler.ngmm.modules[2].output, zipf_distribution)
    local ngmm_lin = ed_coupler.ngmm.modules[2]:backward(ed_coupler.ngmm.modules[1].output, ngmm_reg)
    ed_coupler.ngmm.modules[1]:backward(unmaskedEncOut, ngmm_lin)
    ed_coupler.ngmm:backward(unmaskedEncOut, decGradOut)
    local encGrad = ed_coupler:ngmm_to_enc_backward_connect(num_frames)
    ed_coupler.encoder:backward(encInSeq, encGrad)
    -- gradient updates have been accumulated in the seq_len and utterance loops
    local feval_enc = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_enc then
        parameters_enc:copy(x)
      end
      table.insert(enc_gradient, torch.abs(grad_parameters_enc):max())
      --grad_parameters_enc:clamp(-1,1)
      return err+l1_err, grad_parameters_enc
    end
    local feval_ngmm = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_ngmm then
        parameters_ngmm:copy(x)
      end
      table.insert(ngmm_gradient, torch.abs(grad_parameters_ngmm):max())
      --grad_parameters_ngmm:clamp(-1,1)
      return err+l1_err, grad_parameters_ngmm
    end
    local feval_dec = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_dec then
        parameters_dec:copy(x)
      end
      table.insert(dec_gradient, torch.abs(grad_parameters_dec):max())
      --grad_parameters_dec:clamp(-1,1)
      return err+l1_err, grad_parameters_dec
    end
    -- update network parameters
    optim.adadelta(feval_dec, parameters_dec, {}, dec_state) -- using default config
    optim.adadelta(feval_ngmm, parameters_ngmm, {}, ngmm_state) -- using default config
    optim.adadelta(feval_enc, parameters_enc, {}, enc_state) 
  end -- end for utterance
  training_loss[epoch] = epoch_err / total_num_frames -- normalize error per batch
  l1_loss[epoch] = l1_err / #(data_loader.data)
  if log_loss then loss_logfile:write(training_loss[#training_loss] .. '\n') end
  -- end TRAINING
  ---[[ VALIDATION
  ed_coupler.decoder:evaluate() -- don't switch the encoder to evaluate, otherwise it forgets the embeddings it computed during the sequence, and we lose the information for forward_connect
  local data, num_frames = data_loader:make_batch(1, val_data_index)
  if data then
    -- The input sentences to the encoder. 
    local encInSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then encInSeq:cuda() end
    -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
    local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
    if opt.gpu then decInSeq:cuda() end
    -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
    local decOutSeq = data:clone() -- seq_len x batch_size x dimension
    if opt.gpu then decOutSeq:cuda() end
    -- Forward pass
    local encOut = ed_coupler.encoder:forward(encInSeq)
    local unmaskedEncOut = torch.zeros(encOut:size(2), encOut:size(3)) -- batch_size x dimension
    for input,last_index in pairs(num_frames) do unmaskedEncOut[input] = encOut[last_index][input] end
    ed_coupler.ngmm:forward(unmaskedEncOut)
    ed_coupler:ngmm_to_dec_forward_connect(num_frames)
    local decOut = ed_coupler.decoder:forward(decInSeq)
    local err = mz_criterion:forward(decOutSeq, decOut) -- this ignores zero vectors in decOut for err and grad computation. In this implementation we use the fact that in MSECriterion, the error is symmetric in the input and target, and the gradient is an odd function of (input - target). Exchanging target and input allows the masking to take effect.
    local total_length = 0
    for _,length in pairs(num_frames) do total_length = total_length + length end
    err = err/total_length -- normalize by actual number of frames
    validation_loss[epoch] = err
    if log_exp then exp_logfile:write(string.format('Validation epoch %d ; MSE err = %f\n', epoch, err)) end
  end -- end if data
  -- end VALIDATION ]]
  if log_loss then loss_logfile:flush() end
  if log_exp then exp_logfile:flush() end
  xlua.progress(epoch, opt.epochs)
  collectgarbage()
end -- end for epoch
-- one last forward for the activations
local data, num_frames = data_loader:make_batch(1, #(data_loader.data))
-- The input sentences to the encoder. 
local encInSeq = data:clone() -- seq_len x batch_size x dimension
if opt.gpu then encInSeq:cuda() end
-- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
if opt.gpu then decInSeq:cuda() end
-- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
local decOutSeq = data:clone() -- seq_len x batch_size x dimension
if opt.gpu then decOutSeq:cuda() end
-- Forward pass
local encOut = ed_coupler.encoder:forward(encInSeq)
local unmaskedEncOut = torch.zeros(encOut:size(2), encOut:size(3)) -- batch_size x dimension
for input,last_index in pairs(num_frames) do unmaskedEncOut[input] = encOut[last_index][input] end
--[[
print('encoder activations')
print(unmaskedEncOut)
--]]
ed_coupler.ngmm:forward(unmaskedEncOut)
print('gmm activations')
print(ed_coupler.ngmm.modules[2].output)
--[[
print('mean activations')
print(torch.mean(ed_coupler.ngmm.modules[2].output,1))
--]]
-- save final model
local enc_cpu = enc:clone(); enc_cpu:double()
local dec_cpu = dec:clone(); dec_cpu:double()
local ngmm_cpu = ngmm:clone(); ngmm_cpu:double()
local encLSTM_cpu = {}
local decLSTM_cpu = {}
for layer=1,#encLSTM do
  encLSTM_cpu[layer] = encLSTM[layer]:clone():double()
  decLSTM_cpu[layer] = decLSTM[layer]:clone():double()
end
torch.save(opt.save_file , {encoder = enc_cpu, encoderLSTM = encLSTM_cpu, decoder = dec_cpu, decoderLSTM = decLSTM_cpu, ngmm = ngmm_cpu, opt = opt, training_loss=training_loss, validation_loss=validation_loss})
if log_loss then loss_logfile:close() end
if log_exp then exp_logfile:close() end
if opt.plot and opt.plot ~= 0 then
training_loss_deriv = {}
for ii=1,#training_loss-1 do table.insert(training_loss_deriv, training_loss[ii+1] - training_loss[ii]) end
-- plot training and validation errors
torch.setdefaulttensortype('torch.FloatTensor') -- otherwise get a cuda error
gnuplot.figure(1)
gnuplot.title('training loss')
gnuplot.plot({string.format('training'), torch.Tensor(training_loss)})
---[[
gnuplot.figure(2)
gnuplot.title('training loss zoom')
gnuplot.plot({string.format('training'), torch.Tensor(training_loss):sub(math.floor(#training_loss*0.99),#training_loss)})
if #validation_loss > 0 then 
gnuplot.figure(3)
gnuplot.title('validation loss')
gnuplot.plot({string.format('validation'), torch.Tensor(validation_loss)})
gnuplot.figure(4)
gnuplot.title('validation loss zoom')
gnuplot.plot({string.format('validation'), torch.Tensor(validation_loss):sub(math.floor(#validation_loss*0.99),#validation_loss)})
end
--]]
gnuplot.figure(5)
gnuplot.title('training loss deriv')
gnuplot.plot({string.format('training'), torch.Tensor(training_loss_deriv)})
gnuplot.figure(6)
gnuplot.title('training loss deriv zoom')
gnuplot.plot({string.format('training'), torch.Tensor(training_loss_deriv):sub(math.floor(#training_loss_deriv*0.99),#training_loss_deriv)})
--[[
gnuplot.figure(7)
gnuplot.title('gradients')
gnuplot.plot({string.format('encoder'), torch.Tensor(enc_gradient)}, {string.format('decoder'), torch.Tensor(dec_gradient)})
--]]
gnuplot.figure(8)
gnuplot.title('l1 loss')
gnuplot.plot({string.format('l1 training'), torch.Tensor(l1_loss)})
end -- end if
print(training_loss[opt.epochs] + l1_loss[opt.epochs])
