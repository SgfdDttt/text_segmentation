package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
local GMM = require 'text_segmentation/scripts/gmm'
local ADL = require 'rnn_autoencoder/audio_data_loader'
local EDC = require 'rnn_autoencoder/encoder_decoder_coupler'
require 'gnuplot'
require 'rnn'

opt=lapp[[
-d, --directory (default "")
-L, --list (default "")
-b, --boundaries (default "")
-n, --network (default "")
-G, --gmm (default "")
]]

local tens_to_str = function(tens)
  local buffer = {}
  for line=1,tens:size(1) do
    local frame={}
    for coef=1,tens:size(2) do
      frame[coef] = tens[line][coef]
    end
    buffer[line] = table.concat(frame, ' ')
  end
  return table.concat(buffer, ' ')
end

-- load net
local saved = torch.load(opt.network)
local edc = EDC:new(saved.encoder, saved.encoderLSTM, saved.decoder, saved.decoderLSTM)
-- load gmm
local gmm = GMM:new(opt.gmm)
gmm:load_parameters()
-- load speech segments
local adl = ADL:new(opt.directory, opt.list)
adl:load_mfcc_dictionary(opt.boundaries, 0.0, 1.0)
-- remember unique segments + count how often they appear
local counts = {}
local str_to_tens = {}
local total_counts = 0
for _,seq in pairs(adl.data) do -- seq is a tensor
  local str = tens_to_str(seq)
  counts[str] = (counts[str] or 0) + 1
  total_counts=total_counts+1
  str_to_tens[str] = seq:clone()
end -- end for seq
local real_p = {}
local gmm_p = {}
--print(counts)
-- for each segment compute prob
for str in pairs(counts) do
  counter = (counter or 0) + 1
  local embedding = edc:audio_embedding(str_to_tens[str])
  table.insert(gmm_p, gmm:probability(embedding))
  table.insert(real_p, counts[str]/total_counts)
end
gnuplot.figure(1)
gnuplot.xlabel('empirical prob')
gnuplot.ylabel('gmm prob')
gnuplot.plot({string.format('probabilities - %d points', counter), torch.Tensor(real_p), torch.Tensor(gmm_p), '+'})
print(real_p)
print(gmm_p)
-- correlation etc
linear_regression = {}
local x_tens = torch.Tensor(real_p)
local y_tens = torch.Tensor(gmm_p)
local xy = torch.cmul(x_tens,y_tens):sum()
local xx = torch.cmul(x_tens,x_tens):sum()
local yy = torch.cmul(y_tens,y_tens):sum()
local n = x_tens:size(1)
linear_regression[2] = (n*xy - x_tens:sum()*y_tens:sum()) / (n*xx - (x_tens:sum())^2) -- beta
linear_regression[1] = y_tens:mean() - linear_regression[2] * x_tens:mean() -- alpha
linear_regression[3] = linear_regression[2] * math.sqrt((n*xx - (x_tens:sum())^2)/(n*yy - (y_tens:sum())^2)) -- r
print(string.format('slope %f, intercept %f, squared residuals %f', linear_regression[2], linear_regression[1], linear_regression[3]))
