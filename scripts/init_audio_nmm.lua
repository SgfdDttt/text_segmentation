package.path=package.path .. ';/Users/Nils/Documents/workspace/?.lua' .. ';/home/nholzenb/Documents/workspace/?.lua'
require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require 'rnn_autoencoder/audio_data_loader'
local Coupler = require 'rnn_autoencoder/encoder_decoder_coupler'
pcall(require,'../rnn/SeqLSTM')

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-L,--list               (default "") list of audio files
-d,--directory          (default "") where the audio files are stored
-E,--enc_size        (default 128)             number of units in hidden layers of encoder RNN
-D,--dec_size         (default 128)       number of units in hidden layers of decoder RNN
-l,--layers             (default 2)               number of layers for both nets
-K,--num_clusters       (default 8)
]]

-- load data
local data_loader = DataLoader:new(opt.directory, opt.list)
data_loader:load_mfcc()
print('dimension: ' .. data_loader:dim())

-- Encoder
local enc = nn.Sequential()
enc:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.enc_size)))
local encLSTM = {}
for layer=1,opt.layers do
  encLSTM[layer] = nn.SeqLSTM(opt.enc_size, opt.enc_size)
  enc:add(encLSTM[layer])
end
enc:remember('neither')

-- Transfer layer (NGMM)
local ngmm = nn.Sequential()
ngmm:add(nn.Linear(opt.enc_size, opt.num_clusters))
ngmm:add(nn.Sigmoid())
ngmm:add(nn.Linear(opt.num_clusters, opt.dec_size))

-- Decoder
local dec = nn.Sequential()
dec:add(nn.Sequencer(nn.Linear(data_loader:dim(), opt.dec_size)))
local decLSTM = {}
for layer=1,opt.layers do
  decLSTM[layer] = nn.SeqLSTM(opt.dec_size, opt.dec_size)
  dec:add(decLSTM[layer])
end
dec:add(nn.Sequencer(nn.Linear(opt.dec_size, data_loader:dim())))
dec:remember('neither')

print('encoder')
print(enc)
print('transfer layer')
print(ngmm)
print('decoder')
print(dec)

-- Coupler
local ed_coupler = Coupler:new(enc, encLSTM, dec, decLSTM, ngmm)
local parameters_enc = ed_coupler.encoder:getParameters()
local parameters_dec = ed_coupler.decoder:getParameters()
local parameters_ngmm = ed_coupler.ngmm:getParameters()
parameters_enc:fill(0)
parameters_dec:fill(0)
parameters_ngmm:fill(0)

-- do 1 iteration to give the relevant parameters an initial size (helps when reloading the networks for training)
ed_coupler.encoder:zeroGradParameters()
ed_coupler.decoder:zeroGradParameters()
local data, num_frames = data_loader:make_batch(1, 1)
-- The input sentences to the encoder. 
local encInSeq = data:clone() -- seq_len x batch_size x dimension
-- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
local decInSeq = torch.cat(torch.zeros(1, data:size(2), data:size(3)), data:sub(1,data:size(1)-1):clone(), 1) -- seq_len x batch_size x dimension
-- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
local decOutSeq = data:clone() -- seq_len x batch_size x dimension
-- Forward pass
local encOut = ed_coupler.encoder:forward(encInSeq)
local unmaskedEncOut = torch.zeros(encOut:size(2), encOut:size(3)) -- batch_size x dimension
for input,last_index in pairs(num_frames) do unmaskedEncOut[input] = encOut[last_index][input] end
ed_coupler.ngmm:forward(unmaskedEncOut)
ed_coupler:ngmm_to_dec_forward_connect()
local decOut = ed_coupler.decoder:forward(decInSeq)
-- Backward pass
local gradOutput = torch.zeros(decOut:size())
ed_coupler.decoder:backward(decInSeq, gradOutput)
local decGradOut = ed_coupler:dec_to_ngmm_backward_connect()
ed_coupler.ngmm:backward(unmaskedEncOut, decGradOut)
local encGrad = ed_coupler:ngmm_to_enc_backward_connect(num_frames)
ed_coupler.encoder:backward(encInSeq, encGrad)
-- back to initial values
parameters_enc:uniform(-0.01, 0.01)
parameters_dec:uniform(-0.01, 0.01)
parameters_ngmm:uniform(-0.01, 0.01)

-- save final model
local enc_cpu = enc:clone(); enc_cpu:double()
local dec_cpu = dec:clone(); dec_cpu:double()
local ngmm_cpu = ngmm:clone(); ngmm_cpu:double()
local encLSTM_cpu = {}
local decLSTM_cpu = {}
for layer=1,#encLSTM do
  encLSTM_cpu[layer] = encLSTM[layer]:clone():double()
  decLSTM_cpu[layer] = decLSTM[layer]:clone():double()
end
torch.save(opt.save_file, {encoder = enc_cpu, encoderLSTM = encLSTM_cpu, decoder = dec_cpu, decoderLSTM = decLSTM_cpu, ngmm = ngmm_cpu, opt = opt})
