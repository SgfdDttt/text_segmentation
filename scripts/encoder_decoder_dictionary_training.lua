--[[
--Example of "coupled" separate encoder and decoder networks using SeqLSTM, e.g. for sequence-to-sequence networks.
--]]

require 'nn'
require 'rnn'
require 'optim'
local DataLoader = require '../../rnn_autoencoder/data_loader'
local Coupler = require '../../rnn_autoencoder/encoder_decoder_coupler'
local CorrelationTool = require 'scripts/correlation_tool'
pcall(require, '../../rnn/SeqLSTM')

opt = lapp[[
-s,--save_file          (default "autoencoder")   filename for network
-D,--dictionary         (default "") dictionary file name for training
-n,--network            (default "")              reload pretrained network
-r,--learning_rate      (default 0.001)            learning rate
-u,--unlearn            (default 0)           learn or unlearn the dictionary
-t,--threads            (default 4)               number of threads
-g,--gpu                (default -1)              gpu to run on (default cpu)
-M,--max_len            (default 10)          upper bound on the length of the sequences to train on
--epochs                (default 5)             number of epochs to train for
-b,--batch_size         (default 128)            batch size
-f,--loss_log           (default "loss.log")      loss file
-o,--exp_log            (default "exp.log")       log per epoch
-c,--L2coef            (default 0)            weight regularization
--]]

-- set up gpu
if opt.gpu >= 0 then
  local ok, cunn = pcall(require, 'cunn')
  local ok2, cutorch = pcall(require, 'cutorch')
  if not ok then print('package cunn not found!') end
  if not ok2 then print('package cutorch not found!') end
  if ok and ok2 then
    print('using CUDA on GPU ' .. opt.gpu .. '...')
    cutorch.setDevice(opt.gpu + 1) -- note +1 to make it 0 indexed! sigh lua
    torch.setdefaulttensortype('torch.CudaTensor')
  else
    print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
    print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
    print('Falling back on CPU mode')
    opt.gpu = nil -- overwrite user setting
  end
else
  opt.gpu = nil
end

if opt.unlearn ~= 0 then opt.unlearn = 1 end

-- open log files
--loss_logfile = assert(io.open('logs/' .. opt.loss_log, 'w'))
--exp_logfile = assert(io.open('logs/' .. opt.exp_log, 'w'))

if opt.network then
  print('load saved net ' .. opt.network)
  save_state = torch.load(opt.network)
else
  print('net not found')
end

-- load autoencoder
local coupler = Coupler:new(save_state.encoder, save_state.encoderLSTM, save_state.decoder, save_state.decoderLSTM)
if opt.gpu then
  coupler:cuda()
end
local parameters_enc, grad_parameters_enc = coupler.encoder:getParameters()
local parameters_dec, grad_parameters_dec = coupler.decoder:getParameters()

local criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())

-- load data
local data_loader = DataLoader:new()
data_loader.char_to_int = save_state.char_to_int
data_loader.int_to_char = save_state.int_to_char
local max_seq_len = data_loader:load_dataset_from_dictionary(opt.dictionary)

-- correlation tool
local correlation_tool = CorrelationTool:new(opt.dictionary, data_loader.char_to_int, data_loader.int_to_char)
correlation_tool:load_dictionary()

local criterion = nn.SequencerCriterion(nn.ClassNLLCriterion()) 
criterion.sizeAverage=false -- otherwise loss gets averaged over minibatches, and longer words get an advantage during the training as there are fewer of them per batch
-- adadelta states
local enc_state, dec_state = {}, {}

for epoch=1,opt.epochs do
  -- training
  local train_err = 0
  coupler.encoder:training()
  coupler.decoder:training()
  local batches = data_loader:randomize() -- scrambles the dataset randomly
  for ii=1,#batches,opt.batch_size do
    coupler.encoder:zeroGradParameters()
    coupler.decoder:zeroGradParameters()
    local glob_err = 0
    local batch = {} -- batch[ii] contains all sequences of length ii that are in batches between indices ii and ii+opt.batch_size-1
    for jj=ii,math.min(ii+opt.batch_size-1,#batches) do
      local length = #(batches[jj])
      batch[length] = batch[length] or {}
      table.insert(batch[length], batches[jj])
    end -- end for 
    for index in pairs(batch) do
      if (index > opt.max_len) then break end -- this is an assumption on the length of the longest word
      local data = batch[index]
      -- The input sentences to the encoder. 
      local encInSeq = torch.Tensor(data):t(1,2)
      if opt.gpu then encInSeq:cuda() end
      -- The input sentences to the decoder. Add <s> at start of each sentence as a start sequence symbol. Why is that the inp?
      for ind, seq in pairs(data) do
        local seq2={}
        for ii=1,#seq do
          seq2[ii+1] = seq[ii]
        end
        seq2[1] = data_loader.char_to_int['<s>']
        data[ind] = seq2
      end -- end for ind,seq
      local decInSeq = torch.Tensor(data):t(1,2)
      if opt.gpu then decInSeq:cuda() end
      -- The expected output from the decoder (it will return one character per time-step). Add <eos> at end of each sentence as a stop symbol. Remove <s> at the beginning.
      for ind, seq in pairs(data) do
        local seq2 = {}
        for ii=2,#seq do
          seq2[ii-1] = seq[ii]
        end
        seq2[#seq2+1] = data_loader.char_to_int['<eos>']
        data[ind] = seq2
      end -- end for ind,seq
      local decOutSeq = torch.Tensor(data)
      if opt.gpu then decOutSeq:cuda() end
      -- The decoder predicts one per timestep, so we split accordingly.
      decOutSeq = nn.SplitTable(1, 1):forward(decOutSeq)
      -- Forward pass
      local encOut = coupler.encoder:forward(encInSeq)
      coupler:forwardConnect()
      local decOut = coupler.decoder:forward(decInSeq)
      local err = criterion:forward(decOut, decOutSeq)
      glob_err = glob_err + err --+ coupler.encoder.modules[#coupler.encoder.modules].module.recurrentModule.loss -- accumulate error
      -- Backward pass
      local gradOutput = criterion:backward(decOut, decOutSeq)
      coupler.decoder:backward(decInSeq, gradOutput)
      coupler:backwardConnect()
      local zeroTensor = torch.Tensor(encOut):zero()
      coupler.encoder:backward(encInSeq, zeroTensor)
      --print(coupler.encoder.modules[3].output:min() .. ' ' .. coupler.encoder.modules[3].output:max())
      --print(coupler.encoder.modules[3].module.recurrentModule.loss)
      --print(coupler.encoder.modules[3].gradInput:min() .. ' ' .. coupler.encoder.modules[3].gradInput:max())
    end -- end for _,index
    train_err = train_err + glob_err
    glob_err = glob_err / data_loader:size() -- normalize by total size of data
    -- optionally unlearn the dictionary
    if opt.unlearn == 1 then
      glob_err = -glob_err
      grad_parameters_enc:mul(-1)
      grad_parameters_dec:mul(-1)
    end
    -- gradient updates have been accumulated over previous _,index loop
    local feval_enc = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_enc then
        parameters_enc:copy(x)
      end
      return glob_err, grad_parameters_enc
    end
    local feval_dec = function(x)
      collectgarbage(); collectgarbage()
      if x ~= parameters_dec then
        parameters_dec:copy(x)
      end
      return glob_err, grad_parameters_dec
    end
    -- update network parameters
    optim.adadelta(feval_enc, parameters_enc, {weightDecay=opt.L2coef}, enc_state)
    optim.adadelta(feval_dec, parameters_dec, {weightDecay=opt.L2coef}, dec_state)
  end -- end for ii
  train_err = train_err / data_loader:size()
  if epoch % 8 == 0 then
    --[[
    correlation_tool:compute_losses_on_dictionary(coupler, opt.gpu)
    local correlation = correlation_tool:compute_correlation()
    print(string.format('slope %f, intercept %f, residuals %f, train_err %f', linear_regression[2], linear_regression[1], linear_regression[3], train_err))
    --]]
    print(string.format('train_err %f', train_err))
  end
  xlua.progress(epoch, opt.epochs)
end -- end for epoch
-- save model, at the end of the training
local enc_cpu = coupler.encoder:clone(); enc_cpu:double()
local dec_cpu = coupler.decoder:clone(); dec_cpu:double()
local encLSTM_cpu = {}
local decLSTM_cpu = {}
for layer=1,#coupler.encoderLSTM do
  encLSTM_cpu[layer] = coupler.encoderLSTM[layer]:clone():double()
  decLSTM_cpu[layer] = coupler.decoderLSTM[layer]:clone():double()
end
torch.save(opt.save_file, {encoder = enc_cpu, encoderLSTM = encLSTM_cpu, decoder = dec_cpu, decoderLSTM = decLSTM_cpu, opt = opt, char_to_int = data_loader.char_to_int, int_to_char = data_loader.int_to_char})
