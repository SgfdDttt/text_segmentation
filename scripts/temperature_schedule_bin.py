import sys
iteration = int(sys.argv[1])
# hot cycles less and less frequent ([1,1]; [1,2]; [1,4]; [1,8]...)
it_in_bin = list()
while iteration > 0:
  it_in_bin.append(iteration % 2)
  iteration = iteration / 2
heat = True
for d in it_in_bin:
  heat = heat and (d == 1)
if heat:
  print(float(len(it_in_bin)))
else:
  print(0.0)
