import sys
word_counts = dict()
for line in open(sys.argv[1],'r'):
  words = line[:-1].split(" ")
  for word in words:
    if word not in word_counts:
      word_counts[word]=0
    word_counts[word]+=1
sorted_words = word_counts.keys()
sorted_words.sort()
for word in sorted_words:
  print(word + " " + str(word_counts[word]))
